package com.epam.itaxi.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.itaxi.command.CommandHelper;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.exception.CommandException;

/**
 * Servlet implementation class Controller
 * 
 * @author Uladzimir Luhin
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {
	private static Logger logger = LogManager.getLogger();
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Uses pattern command, to make some actions on the server and return
	 * correct jsp page. Or return error page, if error has occured.
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String commandName = request.getParameter(AttributeName.COMMAND);
		String pageName;
		try {
			ICommand command = CommandHelper.getCommand(commandName);
			pageName = command.execute(request, response);
		} catch (CommandException e) {
			logger.error(e.getMessage(), e);
			pageName = PageName.ERROR_PAGE;
		}
		if (pageName != PageName.AJAX) {
			request.getRequestDispatcher(pageName).forward(request, response);
		}
	}
}