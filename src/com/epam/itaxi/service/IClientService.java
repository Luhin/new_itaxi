package com.epam.itaxi.service;

import java.util.List;

import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.ServiceException;

/**
 * Makes some service actions, relative to the client, and then, if necessary,
 * calls correspondig DAO
 * 
 * @author Uladzimir Luhin
 *
 */
public interface IClientService {
	/**
	 * Determines current client position.
	 * 
	 * @return String, which contains current street.
	 * @throws ServiceException
	 */
	String determineClientPosition() throws ServiceException;

	/**
	 * Determines current client house number.
	 * 
	 * @return current client house number.
	 */
	int determineClientHouseNumber();

	/**
	 * Find client money amount on his acount.
	 * 
	 * @param clientId
	 *            ID of a client, who balance will be checked
	 * @return money amount of a client
	 * @throws ServiceException
	 */
	int checkClientBalance(int clientId) throws ServiceException;

	/**
	 * Repelnish balance of a client. Add given sum to the client money amount.
	 * 
	 * @param clientId
	 *            ID of a client
	 * @param replenishSum
	 *            sum which must be add to the client account
	 * @throws ServiceException
	 */
	boolean replenishBalance(int clientId, int replenishSum, String cardNumber) throws ServiceException;

	/**
	 * Add a new client to the database.
	 * 
	 * @param name
	 *            client name
	 * @param login
	 *            client login
	 * @param password
	 *            client password
	 * @param email
	 *            client Email
	 * @param cardNum
	 *            client card number
	 * @return ID of a new client
	 * @throws ServiceException
	 */
	int signUpClient(String name, String login, String password, String email, String cardNum) throws ServiceException;

	/**
	 * Checks client balance, is it enogh for makin a trip
	 * 
	 * @param clientId
	 * @return
	 * @throws ServiceException
	 */
	boolean isBalanceEnough(int clientId) throws ServiceException;

	/**
	 * Find all clients from the database.
	 * 
	 * @return List with all clients.
	 * @throws ServiceException
	 */
	List<User> findAllClients() throws ServiceException;
}
