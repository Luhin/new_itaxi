package com.epam.itaxi.service;

import java.util.List;

import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.ServiceException;

/**
 * Makes some service actions, relative to the driver, and then, if necessary,
 * calls correspondig DAO
 * 
 * @author Uladzimir Luhin
 *
 */
public interface IDriverService {
	/**
	 * Find current drivers car model.
	 * 
	 * @param driverId
	 *            ID of a driver, who car should be find
	 * @return String contains car model
	 * @throws ServiceException
	 */
	String findDriverCar(int driverId) throws ServiceException;

	/**
	 * Changes drivers car model to the new car model.
	 * 
	 * @param driverId
	 *            ID of a driver, who car should be changed
	 * @param newCar
	 *            new car model
	 * @throws ServiceException
	 */
	boolean changeDriverCar(int driverId, String newCar, String password) throws ServiceException;

	/**
	 * Add a new driver to the database.
	 * 
	 * @param name
	 *            driver name
	 * @param login
	 *            driver login
	 * @param password
	 *            driver password
	 * @param email
	 *            driver Email
	 * @param carModel
	 *            driver car model
	 * @throws ServiceException
	 */
	void signUpDriver(String name, String login, String password, String email, String carModel)
			throws ServiceException;

	/**
	 * Find all drivers from the database.
	 * 
	 * @return List with all drivers.
	 * @throws ServiceException
	 */
	List<User> findAllDrivers() throws ServiceException;
}
