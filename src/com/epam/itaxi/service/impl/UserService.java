package com.epam.itaxi.service.impl;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import com.epam.itaxi.dao.impl.UserDAO;
import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.DAOException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.IUserService;

public class UserService implements IUserService {
	private final static UserService INSTANCE = new UserService();
	private final static String HASH_FUNCTION = "MD5";

	private UserService() {

	}

	public static UserService getInstance() {
		return INSTANCE;
	}

	public User authorizeUser(String login, String password) throws ServiceException {
		User user = null;
		try {
			String encryptedPassword = encryptPassword(password);
			UserDAO userDAO = UserDAO.getInstance();
			user = userDAO.authorizeUser(login, encryptedPassword);
			return user;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public String findUserRating(int userId) throws ServiceException {
		try {
			UserDAO userDAO = UserDAO.getInstance();
			double rating = userDAO.findUserRating(userId);
			String formattedRating = OrderService.getInstance().formatRating(rating);
			return formattedRating;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public String encryptPassword(String password) throws ServiceException {
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance(HASH_FUNCTION);
			messageDigest.update(password.getBytes(), 0, password.length());
			return new BigInteger(messageDigest.digest()).toString();
		} catch (NoSuchAlgorithmException e) {
			throw new ServiceException("Wrong encrypt algorithm", e);
		}
	}

	public boolean matchExistLogin(String login) throws ServiceException {
		try {
			UserDAO userDAO = UserDAO.getInstance();
			return userDAO.checkMatchExistLogin(login);

		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean isBanned(int userId) throws ServiceException {
		UserDAO userDAO = UserDAO.getInstance();
		try {
			return userDAO.isBanned(userId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void banUser(int userId, ArrayList<User> userList) throws ServiceException {
		try {
			UserDAO userDAO = UserDAO.getInstance();
			userDAO.banUser(userId);
			userList.stream().filter(user -> user.getUserId() == userId).forEach(user -> user.setBanned(true));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void unBanUser(int userId, ArrayList<User> userList) throws ServiceException {
		try {
			UserDAO userDAO = UserDAO.getInstance();
			userDAO.unBanUser(userId);
			userList.stream().filter(user -> user.getUserId() == userId).forEach(user -> user.setBanned(false));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
