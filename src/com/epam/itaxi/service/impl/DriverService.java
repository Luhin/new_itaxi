package com.epam.itaxi.service.impl;

import java.util.ArrayList;
import com.epam.itaxi.dao.impl.DriverDAO;
import com.epam.itaxi.dao.impl.UserDAO;
import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.DAOException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.IDriverService;

public class DriverService implements IDriverService {
	private final static DriverService INSTANCE = new DriverService();

	private DriverService() {

	}

	public static DriverService getInstance() {
		return INSTANCE;
	}

	public String findDriverCar(int driverId) throws ServiceException {
		try {
			DriverDAO driverDAO = DriverDAO.getInstance();
			String driverCar = driverDAO.findDriverCarModel(driverId);
			return driverCar;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean changeDriverCar(int driverId, String newCar, String password) throws ServiceException {
		try {
			String encryptedPassword = UserService.getInstance().encryptPassword(password);
			DriverDAO driverDAO = DriverDAO.getInstance();
			UserDAO userDAO = UserDAO.getInstance();
			if (userDAO.chekUserPassword(driverId, encryptedPassword)) {
				driverDAO.changeDriverCar(driverId, newCar);
				return true;
			} else {
				return false;
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void signUpDriver(String name, String login, String password, String email, String carModel)
			throws ServiceException {
		try {
			DriverDAO driverDAO = DriverDAO.getInstance();
			String encryptedPassword = UserService.getInstance().encryptPassword(password);
			driverDAO.signUpDriver(name, login, encryptedPassword, email, carModel);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public ArrayList<User> findAllDrivers() throws ServiceException {
		ArrayList<User> driverList;
		try {
			DriverDAO driverDAO = DriverDAO.getInstance();
			driverList = driverDAO.findAllDrivers();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return driverList;
	}
}
