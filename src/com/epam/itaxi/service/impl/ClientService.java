package com.epam.itaxi.service.impl;

import java.util.ArrayList;
import java.util.Random;
import com.epam.itaxi.dao.impl.ClientDAO;
import com.epam.itaxi.dao.impl.UserDAO;
import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.DAOException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.IClientService;

public class ClientService implements IClientService {
	private final static ClientService INSTANCE = new ClientService();
	private final static int MAX_HOUSES = 150;

	private ClientService() {
	}

	public static ClientService getInstance() {
		return INSTANCE;
	}

	public String determineClientPosition() throws ServiceException {
		String userPosition;
		try {
			ClientDAO clientDAO = ClientDAO.getInstance();
			userPosition = clientDAO.determineClientPosition();
			return userPosition;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public int determineClientHouseNumber() {
		int houseNumber = new Random().nextInt(MAX_HOUSES);
		return houseNumber;
	}

	public int checkClientBalance(int clientId) throws ServiceException {
		int clientBalance;
		try {
			ClientDAO clientDAO = ClientDAO.getInstance();
			clientBalance = clientDAO.checkClientBalance(clientId);
			return clientBalance;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean replenishBalance(int clientId, int replenishSum, String cardNumber) throws ServiceException {
		int cardNum = Integer.parseInt(cardNumber);
		try {
			ClientDAO clientDAO = ClientDAO.getInstance();
			if (!clientDAO.checkCardNumber(clientId, cardNum)) {
				return false;
			} else {
				if (replenishSum >= 100) {
					UserDAO userDAO = UserDAO.getInstance();
					userDAO.unBanUser(clientId);
				}
				clientDAO.replenishBalance(clientId, replenishSum);
				return true;
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public int signUpClient(String name, String login, String password, String email, String cardNum)
			throws ServiceException {
		try {
			ClientDAO clientDAO = ClientDAO.getInstance();
			String encryptedPassword = UserService.getInstance().encryptPassword(password);
			int clientId = clientDAO.signUpClient(name, login, encryptedPassword, email, cardNum);
			return clientId;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean isBalanceEnough(int clientId) throws ServiceException {
		ClientDAO clientDAO = ClientDAO.getInstance();
		try {
			int clientBalance = clientDAO.checkClientBalance(clientId);
			return clientBalance > 10;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public ArrayList<User> findAllClients() throws ServiceException {
		ArrayList<User> clientList;
		try {
			ClientDAO clientDAO = ClientDAO.getInstance();
			clientList = clientDAO.findAllClients();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return clientList;
	}
}
