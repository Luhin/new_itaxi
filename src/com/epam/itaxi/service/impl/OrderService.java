package com.epam.itaxi.service.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.itaxi.dao.impl.ClientDAO;
import com.epam.itaxi.dao.impl.DriverDAO;
import com.epam.itaxi.dao.impl.OrderDAO;
import com.epam.itaxi.dao.impl.UserDAO;
import com.epam.itaxi.entity.Order;
import com.epam.itaxi.entity.OrderKeeper;
import com.epam.itaxi.entity.OrderStatus;
import com.epam.itaxi.entity.UserRole;
import com.epam.itaxi.exception.DAOException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.IOrderService;

public class OrderService implements IOrderService {
	private final static OrderService INSTANCE = new OrderService();
	private final static int MAX_PRICE = 8;
	private final static int SEAT_PRICE = 3;
	private final static int ACCEPT_ORDER_AWAITING_SECONDS = 180;
	private final static int MAX_CAR_AWAITING_MINUTES = 10;
	private final static int MIN_CAR_AWAITING_TIME = 1;
	private final static String SPLITERATOR = "&";
	private final static String NO_CARS = "noCars";
	private final static String ORDER_CANCELLED = "orderCancelled";
	private final static String MONEY_FORMAT = "0.00 BYN";
	private final static String RATING_FORMAT = "#.#";
	private final static String TIME_FORMAT = "0 ���";
	private final static String SUCCESS_ORDER = "successOrder";

	private OrderService() {

	}

	public static OrderService getInstance() {
		return INSTANCE;
	}

	public ArrayList<String> makeStreetList() throws ServiceException {
		ArrayList<String> streetList;
		try {
			OrderDAO orderDAO = OrderDAO.getInstance();
			streetList = orderDAO.makeStreetList();
			Collections.sort(streetList);
			return streetList;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean createOrder(int clientId, String currentStreet, int currentHouseNumber, String destination,
			String destHouseNumber) throws ServiceException {
		String clientName = null;
		double clientRating;
		try {
			UserDAO userDAO = UserDAO.getInstance();
			clientName = userDAO.findUserName(clientId);
			clientRating = userDAO.findUserRating(clientId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		Order order = new Order();
		order.setOrderStatus(OrderStatus.NEW);
		order.setClientId(clientId);
		order.setClientName(clientName);
		String formattedRating = formatRating(clientRating);
		order.setClientRating(formattedRating);
		order.setPosition(currentStreet);
		order.setPositionHouse(currentHouseNumber);
		order.setDestination(destination);
		order.setDestinationHouse(Integer.parseInt(destHouseNumber));
		OrderKeeper orderKeeper = OrderKeeper.getInstance();
		orderKeeper.putClientOrder(clientId, order);
		return true;
	}

	public String formatRating(double rating) {
		DecimalFormat decimalFormat = new DecimalFormat(RATING_FORMAT);
		return decimalFormat.format(rating);
	}

	public ArrayList<Order> showOrders() {
		ArrayList<Order> orderList = new ArrayList<>();
		OrderKeeper orderKeeper = OrderKeeper.getInstance();
		for (Map.Entry<Integer, Order> entry : orderKeeper.getOrders().entrySet()) {
			Order order = entry.getValue();
			if (order.getOrderStatus() == OrderStatus.NEW) {
				orderList.add(order);
			}
		}
		return orderList;
	}

	public boolean acceptOrder(int clientId, int driverId) throws ServiceException {
		OrderKeeper orderKeeper = OrderKeeper.getInstance();
		Order order = orderKeeper.getClientOrder(clientId);
		if (order == null) {
			return false;
		}
		ReentrantLock orderLock = order.getLock();
		try {
			if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
				try {
					if (order.getOrderStatus() != OrderStatus.NEW) {
						return false;
					}
					String driverName = null;
					String carModel = null;
					double driverRating;
					try {
						UserDAO userDAO = UserDAO.getInstance();
						DriverDAO driverDAO = DriverDAO.getInstance();
						driverName = userDAO.findUserName(driverId);
						carModel = driverDAO.findDriverCarModel(driverId);
						driverRating = userDAO.findUserRating(driverId);
					} catch (DAOException e) {
						throw new ServiceException(e);
					}
					order.setDriverId(driverId);
					order.setDriverName(driverName);
					String formettedRating = formatRating(driverRating);
					order.setDriverRating(formettedRating);
					order.setDriverCarModel(carModel);
					double orderPrice = new Random().nextInt(MAX_PRICE) + SEAT_PRICE;
					order.setPrice(orderPrice);
					int carAwaiting = new Random().nextInt(MAX_CAR_AWAITING_MINUTES) + MIN_CAR_AWAITING_TIME;
					order.setTimeCarAwaiting(carAwaiting);
					order.setOrderStatus(OrderStatus.ACCEPTED);
					return true;
				} finally {
					orderLock.unlock();
				}
			} else {
				return false;
			}

		} catch (InterruptedException e) {
			throw new ServiceException("Thread was interrupted", e);
		}
	}

	public void deleteOrder(int userId) throws ServiceException {
		OrderKeeper orderKeeper = OrderKeeper.getInstance();
		Order order = orderKeeper.getClientOrder(userId);
		if (order != null) {
			ReentrantLock orderLock = order.getLock();
			try {
				if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
					try {
						orderKeeper.deleteClientOrder(userId);
					} finally {
						orderLock.unlock();
					}
				}
			} catch (InterruptedException e) {
				throw new ServiceException("Thread was interrupted", e);
			}
		}
	}

	public String checkOrderResult(int orderId) throws ServiceException {
		String result = null;
		OrderKeeper orderKeeper = OrderKeeper.getInstance();
		int checkCount = 0;
		while (true) {
			if (checkCount > ACCEPT_ORDER_AWAITING_SECONDS) {
				Order order = orderKeeper.getClientOrder(orderId);
				if (order == null) {
					result = ORDER_CANCELLED;
					return result;
				}
				ReentrantLock orderLock = order.getLock();
				try {
					if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
						try {
							orderKeeper.deleteClientOrder(orderId);
						} finally {
							orderLock.unlock();
						}
					}
				} catch (InterruptedException e) {
					throw new ServiceException("Thread was interrupted", e);
				}
				result = ORDER_CANCELLED;
				return result;
			}
			try {
				TimeUnit.MILLISECONDS.sleep(300);
				checkCount++;
			} catch (InterruptedException e) {
				throw new ServiceException("Thread was interrupted", e);
			}
			Order order = orderKeeper.getClientOrder(orderId);
			if (order == null) {
				result = ORDER_CANCELLED;
				return result;
			}
			ReentrantLock orderLock = order.getLock();
			try {
				if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
					try {
						if (order.getOrderStatus() == OrderStatus.CONFIRMED) {
							OrderDAO orderDAO = OrderDAO.getInstance();
							if (orderDAO.appendOrder(order)) {
								order.setOrderStatus(OrderStatus.DONE);
								result = SUCCESS_ORDER;
							} else {
								result = ORDER_CANCELLED;
							}
							return result;
						}
					} finally {
						orderLock.unlock();
					}
				}
			} catch (InterruptedException | DAOException e) {
				throw new ServiceException("Order wasn't append", e);
			}
		}
	}

	public String waitOrderAcceptance(int clientId) throws ServiceException {
		String answer = null;
		OrderKeeper orderKeeper = OrderKeeper.getInstance();
		int checkCount = 0;
		while (true) {
			if (checkCount > ACCEPT_ORDER_AWAITING_SECONDS) {
				Order order = orderKeeper.getClientOrder(clientId);
				if (order == null) {
					answer = ORDER_CANCELLED;
					return answer;
				}
				ReentrantLock orderLock = orderKeeper.getClientOrder(clientId).getLock();
				try {
					if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
						try {
							orderKeeper.deleteClientOrder(clientId);
						} finally {
							orderLock.unlock();
						}
					}
				} catch (InterruptedException e) {
					throw new ServiceException("Thread was interrupted", e);
				}
				answer = NO_CARS;
				return answer;
			}
			try {
				TimeUnit.MILLISECONDS.sleep(300);
				checkCount++;
			} catch (InterruptedException e) {
				throw new ServiceException("Thread was interrupted", e);
			}
			Order order = orderKeeper.getClientOrder(clientId);
			if (order == null) {
				answer = ORDER_CANCELLED;
				return answer;
			}
			ReentrantLock orderLock = order.getLock();
			try {
				if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
					try {
						if (order.getOrderStatus() == OrderStatus.ACCEPTED) {
							answer = createAnswerOrder(order);
							return answer;
						}
					} finally {
						orderLock.unlock();
					}
				}
			} catch (InterruptedException e) {
				throw new ServiceException("Thread was interrupted", e);
			}
		}
	}

	public boolean confirmOrder(int orderId) throws ServiceException {
		OrderKeeper orderKeeper = OrderKeeper.getInstance();
		Order order = orderKeeper.getClientOrder(orderId);
		if (order == null) {
			return false;
		}
		ReentrantLock orderLock = order.getLock();
		try {
			if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
				try {
					order.setOrderStatus(OrderStatus.CONFIRMED);
					return true;
				} finally {
					orderLock.unlock();
				}
			} else {
				return false;
			}

		} catch (InterruptedException e) {
			throw new ServiceException("Thread was interrupted", e);
		}
	}

	public boolean doneOrder(int orderId) throws ServiceException {
		OrderKeeper orderKeeper = OrderKeeper.getInstance();
		Order order = orderKeeper.getClientOrder(orderId);
		int checkCount = 0;
		while (true) {
			if (checkCount > ACCEPT_ORDER_AWAITING_SECONDS) {
				return false;
			}
			try {
				TimeUnit.MILLISECONDS.sleep(200);
				checkCount++;
			} catch (InterruptedException e) {
				throw new ServiceException("Thread was interrupted", e);
			}
			if (order == null) {
				return false;
			}
			ReentrantLock orderLock = order.getLock();
			try {
				if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
					try {
						if (order.getOrderStatus() == OrderStatus.DONE) {
							orderKeeper.deleteClientOrder(orderId);
							return true;
						}
					} finally {
						orderLock.unlock();
					}
				} else {
					return false;
				}

			} catch (InterruptedException e) {
				throw new ServiceException("Thread was interrupted", e);
			}
		}
	}

	public String createAnswerOrder(Order order) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(formatTime(order.getTimeCarAwaiting()));
		stringBuffer.append(SPLITERATOR);
		stringBuffer.append(order.getDriverName());
		stringBuffer.append(SPLITERATOR);
		stringBuffer.append(order.getDriverRating());
		stringBuffer.append(SPLITERATOR);
		stringBuffer.append(order.getDriverCarModel());
		stringBuffer.append(SPLITERATOR);
		stringBuffer.append(formatMoney(order.getPrice()));
		return stringBuffer.toString();
	}

	public String formatMoney(double price) {
		DecimalFormat decimalFormat = new DecimalFormat(MONEY_FORMAT);
		return decimalFormat.format(price);
	}

	public String formatTime(int time) {
		DecimalFormat decimalFormat = new DecimalFormat(TIME_FORMAT);
		return decimalFormat.format(time);
	}

	public int findClientRideQuantity(int userId) throws ServiceException {
		int driveCount;
		try {
			OrderDAO orderDAO = OrderDAO.getInstance();
			driveCount = orderDAO.findClientRideQuantity(userId);
			return driveCount;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public int findDriverOrderQuantity(int userId) throws ServiceException {
		int orderCount;
		try {
			OrderDAO orderDAO = OrderDAO.getInstance();
			orderCount = orderDAO.findDriverOrderQuantity(userId);
			return orderCount;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public ArrayList<Order> findClientOrder(int userId) throws ServiceException {
		try {
			OrderDAO orderDAO = OrderDAO.getInstance();
			ArrayList<Order> orderList = orderDAO.findClientOrder(userId);
			return orderList;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public ArrayList<Order> findDriverOrder(int userId) throws ServiceException {
		try {
			OrderDAO orderDAO = OrderDAO.getInstance();
			ArrayList<Order> orderList = orderDAO.findDriverOrder(userId);
			return orderList;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void editClientComment(int orderId, UserRole userRole, String newComment) throws ServiceException {
		try {
			OrderDAO orderDAO = OrderDAO.getInstance();
			orderDAO.editClientComment(orderId, newComment);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void editDriverComment(int orderId, UserRole userRole, String newComment) throws ServiceException {
		try {
			OrderDAO orderDAO = OrderDAO.getInstance();
			orderDAO.editDriverComment(orderId, newComment);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void rateOrder(UserRole userRole, int userId, String comment, String rating) throws ServiceException {
		OrderDAO orderDAO = OrderDAO.getInstance();
		try {
			if (userRole == UserRole.CLIENT) {
				int driverId = orderDAO.addCommentDriver(userId, comment);
				DriverDAO driverDAO = DriverDAO.getInstance();
				driverDAO.rateDriver(driverId, Integer.parseInt(rating));
			} else {
				int clientId = orderDAO.addCommentClient(userId, comment);
				ClientDAO clientDAO = ClientDAO.getInstance();
				clientDAO.rateClient(clientId, Integer.parseInt(rating));
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
