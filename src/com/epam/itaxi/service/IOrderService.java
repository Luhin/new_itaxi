package com.epam.itaxi.service;

import java.util.List;
import com.epam.itaxi.entity.Order;
import com.epam.itaxi.entity.UserRole;
import com.epam.itaxi.exception.ServiceException;

/**
 * Makes some service actions, relative to the order, and then, if necessary,
 * calls correspondig DAO
 * 
 * @author Uladzimir Luhin
 *
 */
public interface IOrderService {
	/**
	 * Creates a list, contains possible addresse to go.
	 * 
	 * @return list, contains streets name
	 * @throws ServiceException
	 */
	List<String> makeStreetList() throws ServiceException;

	/**
	 * Creates a new order in the system. New order will be with status new.
	 * 
	 * @param clientId
	 *            ID of a client will be the key to order
	 * @param currentStreet
	 *            curren street, where client is
	 * @param currentHouseNumber
	 *            house number, where client is
	 * @param destination
	 *            the street, where clietn would like to go
	 * @param destHouseNumber
	 *            house number
	 * @return true, if order created, false if not
	 * @throws ServiceException
	 */
	boolean createOrder(int clientId, String currentStreet, int currentHouseNumber, String destination,
			String destHouseNumber) throws ServiceException;

	/**
	 * Formates rating of the driver or client, to return it in right view
	 * 
	 * @param rating
	 * @return
	 */
	String formatRating(double rating);

	/**
	 * Shows all new active orders in the system on the current moment.
	 * 
	 * @return list with all active new orders
	 */
	List<Order> showOrders();

	/**
	 * Driver accepts one of the new orders, and waiting answer of the client.
	 * 
	 * @param clientId
	 *            key to the order
	 * @param driverId
	 * @return
	 * @throws ServiceException
	 */
	boolean acceptOrder(int clientId, int driverId) throws ServiceException;

	/**
	 * This method checks the status of the order, and waititng client
	 * confirmation
	 * 
	 * @param orderId
	 *            ID of the order, the same as ID of the client
	 * @return String contains answer of the client
	 * @throws ServiceException
	 */
	String checkOrderResult(int orderId) throws ServiceException;

	/**
	 * When client created the new order, he is waiting answer from one of the
	 * drivers.
	 * 
	 * @param clientId
	 * @return String contains answer of the driver
	 * @throws ServiceException
	 */
	String waitOrderAcceptance(int clientId) throws ServiceException;

	/**
	 * When driver accepts the new order, client looks information about driver
	 * and price, and decides go, or not to go
	 * 
	 * @param orderId
	 * @return
	 * @throws ServiceException
	 */
	boolean confirmOrder(int orderId) throws ServiceException;

	/**
	 * When order is done, order will be saved in the database and deleted from
	 * the system
	 * 
	 * @param orderId
	 * @return
	 * @throws ServiceException
	 */
	boolean doneOrder(int orderId) throws ServiceException;

	/**
	 * When driver has accepted the order, this method will be collect all
	 * information about driver and sends it to the client
	 * 
	 * @param order
	 * @return String with all information about driver and price
	 */
	String createAnswerOrder(Order order);

	/**
	 * Format time from database in miliseconds to users locale.
	 * 
	 * @param time
	 * @return
	 */
	String formatTime(int time);

	/**
	 * Format price of the order to the right format
	 * 
	 * @param price
	 * @return
	 */
	String formatMoney(double price);

	/**
	 * Find total quantity rides of a client.
	 * 
	 * @param userId
	 *            ID of a client, who rides must be calculate
	 * @return total number of trips
	 * @throws ServiceException
	 */
	int findClientRideQuantity(int userId) throws ServiceException;

	/**
	 * Find total quantity rides of a driver.
	 * 
	 * @param userId
	 *            ID of a driver, who rides must be calculate
	 * @return total number of trips
	 * @throws ServiceException
	 */
	int findDriverOrderQuantity(int userId) throws ServiceException;

	/**
	 * Find total number of orders, made by client.
	 * 
	 * @param userId
	 *            ID of a client
	 * @return list with all client orders
	 * @throws ServiceException
	 */
	List<Order> findClientOrder(int userId) throws ServiceException;

	/**
	 * Find total number of orders, made by driver.
	 * 
	 * @param userId
	 *            ID of a driver
	 * @return list with all driver orders
	 * @throws ServiceException
	 */
	List<Order> findDriverOrder(int userId) throws ServiceException;

	/**
	 * If order was done, or cancelled, this method will be delete ordee from
	 * the system.
	 * 
	 * @param userId
	 * @throws ServiceException
	 */
	void deleteOrder(int userId) throws ServiceException;

	/**
	 * Edit a comment to the trip.
	 * 
	 * @param orderId
	 *            ID of an order, which comment should be edited
	 * @param newComment
	 *            new comment, which must be addet to the order
	 * @throws ServiceException
	 */
	void editClientComment(int orderId, UserRole userRole, String newComment) throws ServiceException;

	/**
	 * Edit a comment to the trip.
	 * 
	 * @param orderId
	 *            ID of an order, which comment should be edited
	 * @param newComment
	 *            new comment, which must be addet to the order
	 * @throws ServiceException
	 */
	void editDriverComment(int orderId, UserRole userRole, String newComment) throws ServiceException;

	/**
	 * After successs order system will ask client and driver rate each other
	 * and create comment about trip.
	 * 
	 * @param userRole
	 *            helps system understand who creates comment, to save it in the
	 *            right place
	 * @param userId
	 *            ID of the client or driver
	 * @param comment
	 *            new comment
	 * @param rating
	 *            mark of the trip
	 * @throws ServiceException
	 */
	void rateOrder(UserRole userRole, int userId, String comment, String rating) throws ServiceException;
}
