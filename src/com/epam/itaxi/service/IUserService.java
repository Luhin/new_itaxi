package com.epam.itaxi.service;

import java.util.ArrayList;

import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.ServiceException;

/**
 * Makes some service actions, relative to the all users, and then, if
 * necessary, calls correspondig DAO
 * 
 * @author Uladzimir Luhin
 *
 */
public interface IUserService {
	/**
	 * Authorization of the user in the system.
	 * 
	 * @param login
	 * @param password
	 * @return if login and password are correct, return object User whith all
	 *         necessary data.
	 * @throws ServiceException
	 */
	User authorizeUser(String login, String password) throws ServiceException;

	/**
	 * Find rating of a user;
	 * 
	 * @param userId
	 *            ID of a user
	 * @return user rating
	 * @throws ServiceException
	 */
	String findUserRating(int userId) throws ServiceException;

	/**
	 * Encryts password by corresponding encoding(MD5 for example)
	 * 
	 * @param password
	 * @return String contains encrypted password
	 * @throws ServiceException
	 */
	String encryptPassword(String password) throws ServiceException;

	/**
	 * Check login for the unique. Compare login to the database logins.
	 * 
	 * @param login
	 *            login which must be checked
	 * @return
	 * @throws ServiceException
	 */
	boolean matchExistLogin(String login) throws ServiceException;

	/**
	 * Check banned user or not.
	 * 
	 * @param userId
	 *            ID of a user
	 * @return if user banned - return true, if not return false
	 * @throws ServiceException
	 */
	boolean isBanned(int userId) throws ServiceException;

	/**
	 * Ban user. Banned user cannot make some actions.
	 * 
	 * @param userId
	 *            ID of a user who must be banned
	 * @throws ServiceException
	 */
	void banUser(int userId, ArrayList<User> userList) throws ServiceException;

	/**
	 * Unban user
	 * 
	 * @param userId
	 *            ID of a user, who must be unbanned
	 * @throws ServiceException
	 */
	void unBanUser(int userId, ArrayList<User> userList) throws ServiceException;
}
