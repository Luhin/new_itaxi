package com.epam.itaxi.entity;

/**
 * User is an object, which contains all information about user.
 * 
 * @author Uladzimir Luhin
 *
 */
public class User {
	private int userId;
	private UserRole role;
	private String name;
	private String rating;
	private boolean isBanned;
	private String eMail;
	private String carModel;
	private int orderCount;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(int orderCount) {
		this.orderCount = orderCount;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public boolean isBanned() {
		return isBanned;
	}

	public void setBanned(boolean isBanned) {
		this.isBanned = isBanned;
	}

	public String getEMail() {
		return eMail;
	}

	public void setEMail(String eMail) {
		this.eMail = eMail;
	}

	public String getCarModel() {
		return carModel;
	}

	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = result + ((carModel == null) ? 0 : carModel.hashCode());
		result = result + ((eMail == null) ? 0 : eMail.hashCode());
		result = result + (isBanned ? 1231 : 1237);
		result = result + ((name == null) ? 0 : name.hashCode());
		result = result + orderCount;
		result = result + ((rating == null) ? 0 : rating.hashCode());
		result = result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + userId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		User other = (User) obj;
		if (carModel == null) {
			if (other.carModel != null) {
				return false;
			}
		} else if (!carModel.equals(other.carModel)) {
			return false;
		}
		if (eMail == null) {
			if (other.eMail != null) {
				return false;
			}
		} else if (!eMail.equals(other.eMail)) {
			return false;
		}
		if (isBanned != other.isBanned) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (orderCount != other.orderCount) {
			return false;
		}
		if (rating == null) {
			if (other.rating != null) {
				return false;
			}
		} else if (!rating.equals(other.rating)) {
			return false;
		}
		if (role != other.role) {
			return false;
		}
		if (userId != other.userId) {
			return false;
		}
		return true;
	}
}
