package com.epam.itaxi.entity;

public enum UserRole {
	ADMIN, CLIENT, DRIVER
}
