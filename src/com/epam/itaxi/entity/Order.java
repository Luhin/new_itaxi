package com.epam.itaxi.entity;

import java.util.Calendar;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Order is an object, which contains all necessary information about trip.
 * 
 * @author Uladzimir Luhin
 *
 */
public class Order {
	private int orderId;
	private int driverId;
	private String driverName;
	private String driverCarModel;
	private String driverRating;
	private int clientId;
	private String clientName;
	private String clientRating;
	private double price;
	private Calendar date;
	private String position;
	private int positionHouse;
	private String destination;
	private int destinationHouse;
	private String clientComment;
	private String driverComment;
	private int timeCarAwaiting;
	private OrderStatus orderStatus;
	private ReentrantLock lock;

	public Order() {
		lock = new ReentrantLock();
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getDriverId() {
		return driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public int getTimeCarAwaiting() {
		return timeCarAwaiting;
	}

	public void setTimeCarAwaiting(int timeCarAwaiting) {
		this.timeCarAwaiting = timeCarAwaiting;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Calendar getDate() {
		return date;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getDriverCarModel() {
		return driverCarModel;
	}

	public void setDriverCarModel(String driverCarModel) {
		this.driverCarModel = driverCarModel;
	}

	public String getDriverRating() {
		return driverRating;
	}

	public void setDriverRating(String driverRating) {
		this.driverRating = driverRating;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientRating() {
		return clientRating;
	}

	public void setClientRating(String clientRating) {
		this.clientRating = clientRating;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getPositionHouse() {
		return positionHouse;
	}

	public void setPositionHouse(int positionHouse) {
		this.positionHouse = positionHouse;
	}

	public int getDestinationHouse() {
		return destinationHouse;
	}

	public void setDestinationHouse(int destinationHouse) {
		this.destinationHouse = destinationHouse;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getClientComment() {
		return clientComment;
	}

	public void setClientComment(String clientComment) {
		this.clientComment = clientComment;
	}

	public String getDriverComment() {
		return driverComment;
	}

	public void setDriverComment(String driverComment) {
		this.driverComment = driverComment;
	}

	public ReentrantLock getLock() {
		return lock;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = result + ((clientComment == null) ? 0 : clientComment.hashCode());
		result = prime * result + clientId;
		result = result + ((clientName == null) ? 0 : clientName.hashCode());
		result = result + ((clientRating == null) ? 0 : clientRating.hashCode());
		result = result + ((date == null) ? 0 : date.hashCode());
		result = result + ((destination == null) ? 0 : destination.hashCode());
		result = result + destinationHouse;
		result = result + ((driverCarModel == null) ? 0 : driverCarModel.hashCode());
		result = result + ((driverComment == null) ? 0 : driverComment.hashCode());
		result = result + driverId;
		result = result + ((driverName == null) ? 0 : driverName.hashCode());
		result = result + ((driverRating == null) ? 0 : driverRating.hashCode());
		result = result + ((lock == null) ? 0 : lock.hashCode());
		result = result + orderId;
		result = result + ((orderStatus == null) ? 0 : orderStatus.hashCode());
		result = result + ((position == null) ? 0 : position.hashCode());
		result = result + positionHouse;
		long temp = Double.doubleToLongBits(price);
		result = result + (int) (temp ^ (temp >>> 32));
		result = result + timeCarAwaiting;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Order other = (Order) obj;
		if (clientComment == null) {
			if (other.clientComment != null) {
				return false;
			}
		} else if (!clientComment.equals(other.clientComment)) {
			return false;
		}
		if (clientId != other.clientId) {
			return false;
		}
		if (clientName == null) {
			if (other.clientName != null) {
				return false;
			}
		} else if (!clientName.equals(other.clientName)) {
			return false;
		}
		if (clientRating == null) {
			if (other.clientRating != null) {
				return false;
			}
		} else if (!clientRating.equals(other.clientRating)) {
			return false;
		}
		if (date == null) {
			if (other.date != null) {
				return false;
			}
		} else if (!date.equals(other.date)) {
			return false;
		}
		if (destination == null) {
			if (other.destination != null) {
				return false;
			}
		} else if (!destination.equals(other.destination)) {
			return false;
		}
		if (destinationHouse != other.destinationHouse) {
			return false;
		}
		if (driverCarModel == null) {
			if (other.driverCarModel != null) {
				return false;
			}
		} else if (!driverCarModel.equals(other.driverCarModel)) {
			return false;
		}
		if (driverComment == null) {
			if (other.driverComment != null) {
				return false;
			}
		} else if (!driverComment.equals(other.driverComment)) {
			return false;
		}
		if (driverId != other.driverId) {
			return false;
		}
		if (driverName == null) {
			if (other.driverName != null) {
				return false;
			}
		} else if (!driverName.equals(other.driverName)) {
			return false;
		}
		if (driverRating == null) {
			if (other.driverRating != null) {
				return false;
			}
		} else if (!driverRating.equals(other.driverRating)) {
			return false;
		}
		if (lock == null) {
			if (other.lock != null) {
				return false;
			}
		} else if (!lock.equals(other.lock)) {
			return false;
		}
		if (orderId != other.orderId) {
			return false;
		}
		if (orderStatus != other.orderStatus) {
			return false;
		}
		if (position == null) {
			if (other.position != null) {
				return false;
			}
		} else if (!position.equals(other.position)) {
			return false;
		}
		if (positionHouse != other.positionHouse) {
			return false;
		}
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price)) {
			return false;
		}
		if (timeCarAwaiting != other.timeCarAwaiting) {
			return false;
		}
		return true;
	}

}
