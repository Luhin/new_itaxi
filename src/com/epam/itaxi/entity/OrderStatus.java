package com.epam.itaxi.entity;

public enum OrderStatus {
	NEW, ACCEPTED, CONFIRMED, DONE
}