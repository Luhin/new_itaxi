package com.epam.itaxi.entity;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * OrderKeeper keep and manage all active orders in the system.
 * 
 * @author ��������
 *
 */
public class OrderKeeper {
	/**
	 * instance of a singleton OrderKeeper
	 */
	private static OrderKeeper instance;
	/**
	 * @see java.util.concurrent.atomic.AtomicBoolean
	 */
	private static AtomicBoolean isCreated = new AtomicBoolean(false);
	/**
	 * @see java.util.concurrent.locks.ReentrantLock
	 */
	private static ReentrantLock lock = new ReentrantLock();
	/**
	 * Contains all active orders. Key - is clientId, value - Order.
	 */
	private ConcurrentHashMap<Integer, Order> orders;

	private OrderKeeper() {
		orders = new ConcurrentHashMap<>();
	}

	public static OrderKeeper getInstance() {
		if (!isCreated.get()) {
			try {
				lock.lock();
				if (instance == null) {
					instance = new OrderKeeper();
					isCreated.getAndSet(true);
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}

	public ConcurrentHashMap<Integer, Order> getOrders() {
		return orders;
	}

	/**
	 * Add a new order to the keeper.
	 * 
	 * @param clientId
	 *            ID of a client, which will be the key to the order
	 * @param order
	 */
	public void putClientOrder(Integer clientId, Order order) {
		orders.putIfAbsent(clientId, order);
	}

	public Order getClientOrder(int clientId) {
		Order order = orders.get(clientId);
		return order;
	}

	/**
	 * Removes order from the system. Order removes in case success finishing
	 * order, or if order was cancelled
	 * 
	 * @param clientId
	 *            ID of a client - key to the order
	 */
	public void deleteClientOrder(int clientId) {
		orders.remove(clientId);
	}
}
