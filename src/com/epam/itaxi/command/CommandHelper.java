package com.epam.itaxi.command;

import java.util.EnumMap;

import com.epam.itaxi.command.impl.AcceptOrderCommand;
import com.epam.itaxi.command.impl.AuthorizationCommand;
import com.epam.itaxi.command.impl.BanUserCommand;
import com.epam.itaxi.command.impl.CancelOrderCommand;
import com.epam.itaxi.command.impl.ChangeDriverCarCommand;
import com.epam.itaxi.command.impl.CheckOrderResultCommand;
import com.epam.itaxi.command.impl.ConfirmOrderCommand;
import com.epam.itaxi.command.impl.CreateOrderCommand;
import com.epam.itaxi.command.impl.EditCommentCommand;
import com.epam.itaxi.command.impl.EditingCommentCommand;
import com.epam.itaxi.command.impl.LocalizationCommand;
import com.epam.itaxi.command.impl.LogoutCommand;
import com.epam.itaxi.command.impl.MainPageCommand;
import com.epam.itaxi.command.impl.RateOrderCommand;
import com.epam.itaxi.command.impl.ReplenishBalanceCommand;
import com.epam.itaxi.command.impl.SendOrderCommand;
import com.epam.itaxi.command.impl.ShowAllClientsCommand;
import com.epam.itaxi.command.impl.ShowAllDriversCommand;
import com.epam.itaxi.command.impl.ShowClientOrderCommand;
import com.epam.itaxi.command.impl.ShowClientProfileCommand;
import com.epam.itaxi.command.impl.ShowDriverOrdersCommand;
import com.epam.itaxi.command.impl.ShowDriverProfileCommand;
import com.epam.itaxi.command.impl.ShowOrderListCommand;
import com.epam.itaxi.command.impl.SignUpClientCommand;
import com.epam.itaxi.command.impl.SignUpDriverCommand;
import com.epam.itaxi.command.impl.UnbanUserCommand;
import com.epam.itaxi.command.impl.WaitOrderAcceptanceCommand;
import com.epam.itaxi.exception.CommandException;

public class CommandHelper {
	private static EnumMap<CommandName, ICommand> commands;

	static {
		commands = new EnumMap<CommandName, ICommand>(CommandName.class);
		commands.put(CommandName.AUTHORIZATION, new AuthorizationCommand());
		commands.put(CommandName.LOCALIZATION, new LocalizationCommand());
		commands.put(CommandName.LOGOUT, new LogoutCommand());
		commands.put(CommandName.CREATE_ORDER, new CreateOrderCommand());
		commands.put(CommandName.SHOW_ORDER_LIST, new ShowOrderListCommand());
		commands.put(CommandName.SEND_ORDER, new SendOrderCommand());
		commands.put(CommandName.ACCCEPT_ORDER, new AcceptOrderCommand());
		commands.put(CommandName.WAIT_ACCEPT_ORDER, new WaitOrderAcceptanceCommand());
		commands.put(CommandName.SHOW_CLIENT_PROFILE, new ShowClientProfileCommand());
		commands.put(CommandName.TO_MAIN_PAGE, new MainPageCommand());
		commands.put(CommandName.REPLENISH_BALANCE, new ReplenishBalanceCommand());
		commands.put(CommandName.SHOW_CLIENT_ORDERS, new ShowClientOrderCommand());
		commands.put(CommandName.EDITING, new EditingCommentCommand());
		commands.put(CommandName.EDIT_COMMENT, new EditCommentCommand());
		commands.put(CommandName.CHANGE_CAR, new ChangeDriverCarCommand());
		commands.put(CommandName.SHOW_DRIVER_PROFILE, new ShowDriverProfileCommand());
		commands.put(CommandName.SHOW_DRIVER_ORDERS, new ShowDriverOrdersCommand());
		commands.put(CommandName.SIGN_UP_CLIENT, new SignUpClientCommand());
		commands.put(CommandName.CANCEL_ORDER, new CancelOrderCommand());
		commands.put(CommandName.CONFIRM_ORDER, new ConfirmOrderCommand());
		commands.put(CommandName.CHECK_ORDER_RESULT, new CheckOrderResultCommand());
		commands.put(CommandName.RATE_ORDER, new RateOrderCommand());
		commands.put(CommandName.SIGN_UP_DRIVER, new SignUpDriverCommand());
		commands.put(CommandName.SHOW_ALL_CLIENTS, new ShowAllClientsCommand());
		commands.put(CommandName.SHOW_ALL_DRIVERS, new ShowAllDriversCommand());
		commands.put(CommandName.BAN_USER, new BanUserCommand());
		commands.put(CommandName.UNBAN_USER, new UnbanUserCommand());
	}

	public static ICommand getCommand(String commandName) throws CommandException {
		try {
			CommandName key = CommandName.valueOf(commandName.replace('-', '_').toUpperCase());
			ICommand command = commands.get(key);
			return command;
		} catch (IllegalArgumentException e) {
			throw new CommandException("Wrong command name", e);
		}
	}
}
