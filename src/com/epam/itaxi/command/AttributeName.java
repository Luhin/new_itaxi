package com.epam.itaxi.command;

public class AttributeName {
	public final static String COMMAND = "command";
	public final static String PASSWORD = "password";
	public final static String INVALID_DATA = "invalidData";
	public final static String INVALID_REGISTR_DATA = "invalidRegistrData";
	public final static String SUCCESS_OPERATION = "successOperation";
	public final static String LAST_PAGE = "lastPage";
	public final static String LOCALE = "locale";
	public final static String DESTINATION = "destination";
	public final static String CURRENT_STREET = "currentStreet";
	public final static String CURRENT_HOUSE_NUMBER = "currentHouseNumber";
	public final static String ORDER_LIST = "orderList";
	public final static String USER_ID = "userId";
	public final static String CLIENT_ID = "clientId";
	public final static String USER_NAME = "userName";
	public final static String LOGIN = "login";
	public final static String USER_ROLE = "userRole";
	public final static String STREET_LIST = "streetList";
	public final static String HOUSE_NUMBER = "houseNumber";
	public final static String PREVIOUS_STREET = "previousAddress";
	public final static String PREVIOUS_HOUSE_NUMBER = "previousNumber";
	public final static String USER_RATING = "userRating";
	public final static String CLIENT_BALANCE = "clientBalance";
	public final static String RIDE_QUANTITY = "rideQuantity";
	public final static String CLIENT_DISCOUNT = "clientDiscount";
	public final static String CARD_NUMBER = "cardNumber";
	public final static String REPLENISH_SUM = "replenishSum";
	public final static String ORDER_ID = "orderId";
	public final static String NEW_COMMENT = "newComment";
	public final static String DRIVER_CAR = "driverCar";
	public final static String NEW_CAR = "newCar";
	public final static String REGISTR_NAME = "registrName";
	public final static String REGISTR_LOGIN = "registrLogin";
	public final static String REGISTR_PASS = "registrPass";
	public final static String REGISTR_REPEAT_PASS = "registrRepeatPass";
	public final static String REGISTR_EMAIL = "registrEmail";
	public final static String REGISTR_CARD_NUMBER = "registrCardNumber";
	public final static String MAKE_ORDER_ERROR = "makeOrderError";
	public final static String COMMENT = "comment";
	public final static String RATING = "rating";
	public final static String SUCCESS_ORDER = "successOrder";
	public final static String ERROR_MESSAGE = "errorMessage";
	public final static String NOT_ENOUGH_BALANCE = "notEnoughBalance";
	public final static String REGISTR_CAR_MODEL = "registrCardModel";
	public final static String USER_URL = "userURL";
	public final static String USER_LIST = "userList";
	public final static String PAGE_UNIQUE = "pageUnique";

	private AttributeName() {
	}
}
