package com.epam.itaxi.command.impl;

import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.ClientService;
import com.epam.itaxi.service.impl.OrderService;
import com.epam.itaxi.service.impl.UserService;

public class CreateOrderCommand implements ICommand {
	private final static String LOCALE_PROPERTIES = "property.locale";
	private final static String BANNED_USER = "locale.error.banned";
	private final static String NOT_ENOUG_BALANCE = "locale.error.not.enough.money";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int clientId = (int) session.getAttribute(AttributeName.USER_ID);
		String userLocale = (String) session.getAttribute(AttributeName.LOCALE);
		ClientService clientService = ClientService.getInstance();
		try {
			String errorMessage;
			if (!clientService.isBalanceEnough(clientId)) {
				errorMessage = createErrorMessage(NOT_ENOUG_BALANCE, userLocale);
				request.setAttribute(AttributeName.ERROR_MESSAGE, errorMessage);
				session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_PAGE);
				return PageName.USER_PAGE;
			}
			if (UserService.getInstance().isBanned(clientId)) {
				errorMessage = createErrorMessage(BANNED_USER, userLocale);
				request.setAttribute(AttributeName.ERROR_MESSAGE, errorMessage);
				session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_PAGE);
				return PageName.USER_PAGE;
			}
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		Cookie cookies[] = request.getCookies();
		String previousStreet = null;
		int previousHouseNumber = 0;
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(AttributeName.PREVIOUS_STREET)) {
					previousStreet = cookie.getValue();
				}
				if (cookie.getName().equals(AttributeName.PREVIOUS_HOUSE_NUMBER)) {
					previousHouseNumber = Integer.parseInt(cookie.getValue());
				}
			}
			session.setAttribute(AttributeName.PREVIOUS_STREET, previousStreet);
			session.setAttribute(AttributeName.PREVIOUS_HOUSE_NUMBER, previousHouseNumber);
		}
		ArrayList<String> streetList;
		String currentStreet;
		int currentHouseNumber;
		try {
			currentStreet = clientService.determineClientPosition();
			currentHouseNumber = clientService.determineClientHouseNumber();
			streetList = OrderService.getInstance().makeStreetList();
			session.setAttribute(AttributeName.CURRENT_STREET, currentStreet);
			session.setAttribute(AttributeName.CURRENT_HOUSE_NUMBER, currentHouseNumber);
			session.setAttribute(AttributeName.STREET_LIST, streetList);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		session.setAttribute(AttributeName.LAST_PAGE, PageName.CREATE_ORDER);
		return PageName.CREATE_ORDER;
	}

	private String createErrorMessage(String errorType, String userLocale) {
		Locale locale;
		if (userLocale == null) {
			locale = Locale.getDefault();
		} else {
			locale = new Locale(userLocale);
		}
		ResourceBundle bundle = ResourceBundle.getBundle(LOCALE_PROPERTIES, locale);
		String errorMessage = bundle.getString(errorType);
		return errorMessage;
	}
}
