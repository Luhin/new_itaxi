package com.epam.itaxi.command.impl;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.DriverService;
import com.epam.itaxi.service.impl.UserService;
import com.epam.itaxi.validation.Validation;

public class SignUpDriverCommand implements ICommand {
	private final static String LOCALE_PROPERTIES = "property.locale";
	private final static String LOGIN_EXISTS = "locale.error.login.exists";
	private final static String INCORRECT_NAME = "locale.error.incorrect.name";
	private final static String INCORRECT_LOGIN = "locale.error.incorrect.login";
	private final static String INCORRECT_PASSWORD = "locale.error.incorrect.password";
	private final static String PASSWORDS_DONT_MATCH = "locale.error.pass.dont.match";
	private final static String INCORRECT_IMAIL = "locale.error.incorrect.email";
	private final static String INCORRECT_CAR_MODEL = "locale.error.incorrect.car.model";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		String userLocale = (String) session.getAttribute(AttributeName.LOCALE);
		String registrName = request.getParameter(AttributeName.REGISTR_NAME);
		String registrLogin = request.getParameter(AttributeName.REGISTR_LOGIN);
		String registrPass = request.getParameter(AttributeName.REGISTR_PASS);
		String registrRepeatPass = request.getParameter(AttributeName.REGISTR_REPEAT_PASS);
		String registrEmail = request.getParameter(AttributeName.REGISTR_EMAIL);
		String registrCarModel = request.getParameter(AttributeName.REGISTR_CAR_MODEL);
		String errorMessage = validateData(registrName, registrLogin, registrPass, registrRepeatPass, registrEmail,
				registrCarModel, userLocale);
		if (errorMessage != null) {
			request.setAttribute(AttributeName.INVALID_REGISTR_DATA, errorMessage);
			return PageName.AUTHORIZATION;
		}
		try {
			if (UserService.getInstance().matchExistLogin(registrLogin)) {
				errorMessage = createErrorMessage(LOGIN_EXISTS, userLocale);
				request.setAttribute(AttributeName.INVALID_REGISTR_DATA, errorMessage);
				return PageName.USER_PAGE;
			}
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		try {
			DriverService.getInstance().signUpDriver(registrName, registrLogin, registrPass, registrEmail,
					registrCarModel);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		request.setAttribute(AttributeName.SUCCESS_OPERATION, true);
		session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_PAGE);
		return PageName.USER_PAGE;
	}

	private String createErrorMessage(String errorType, String userLocale) {
		Locale locale;
		if (userLocale == null) {
			locale = Locale.getDefault();
		} else {
			locale = new Locale(userLocale);
		}
		ResourceBundle bundle = ResourceBundle.getBundle(LOCALE_PROPERTIES, locale);
		String errorMessage = bundle.getString(errorType);
		return errorMessage;
	}

	private String validateData(String registrName, String registrLogin, String registrPass, String repeatedPass,
			String registrEmail, String registrCarModel, String userLocale) {
		Validation validation = Validation.getInstance();
		if (!validation.validateName(registrName)) {
			return createErrorMessage(INCORRECT_NAME, userLocale);
		}
		if (!validation.validateLogin(registrLogin)) {
			return createErrorMessage(INCORRECT_LOGIN, userLocale);
		}
		if (!validation.validatePassword(registrPass)) {
			return createErrorMessage(INCORRECT_PASSWORD, userLocale);
		}
		if (!validation.validatePassEqals(registrPass, repeatedPass)) {
			return createErrorMessage(PASSWORDS_DONT_MATCH, userLocale);
		}
		if (!validation.validateEmail(registrEmail)) {
			return createErrorMessage(INCORRECT_IMAIL, userLocale);
		}
		if (!validation.validateCarModel(registrCarModel)) {
			return createErrorMessage(INCORRECT_CAR_MODEL, userLocale);
		} else {
			return null;
		}
	}
}
