package com.epam.itaxi.command.impl;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.OrderService;
import com.epam.itaxi.validation.Validation;

public class SendOrderCommand implements ICommand {
	private final static String LOCALE_PROPERTIES = "property.locale";
	private final static String INCORRECT_HOUSE_NUM = "locale.error.incorrect.house.num";
	private final static int COOKIE_MAX_AGE = 2592000;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int clientId = (int) session.getAttribute(AttributeName.USER_ID);
		String positionStreet = (String) session.getAttribute(AttributeName.CURRENT_STREET);
		int positionHouseNumber = (int) session.getAttribute(AttributeName.CURRENT_HOUSE_NUMBER);
		String destination = request.getParameter(AttributeName.DESTINATION);
		String houseNumber = request.getParameter(AttributeName.HOUSE_NUMBER);
		if (destination != null) {
			session.setAttribute(AttributeName.DESTINATION, destination);
			session.setAttribute(AttributeName.HOUSE_NUMBER, houseNumber);
		} else {
			destination = (String) session.getAttribute(AttributeName.DESTINATION);
			houseNumber = (String) session.getAttribute(AttributeName.HOUSE_NUMBER);
		}
		String userLocale = (String) session.getAttribute(AttributeName.LOCALE);
		if (!Validation.getInstance().validateHouseNumber(houseNumber)) {
			String errorMessage = createErrorMessage(INCORRECT_HOUSE_NUM, userLocale);
			request.setAttribute(AttributeName.INVALID_REGISTR_DATA, errorMessage);
			return PageName.CREATE_ORDER;
		}
		Cookie previousAddressCookie = new Cookie(AttributeName.PREVIOUS_STREET, destination);
		previousAddressCookie.setMaxAge(COOKIE_MAX_AGE);
		response.addCookie(previousAddressCookie);
		Cookie previousHouseCookie = new Cookie(AttributeName.PREVIOUS_HOUSE_NUMBER, houseNumber);
		previousHouseCookie.setMaxAge(COOKIE_MAX_AGE);
		response.addCookie(previousHouseCookie);
		try {
			OrderService.getInstance().createOrder(clientId, positionStreet, positionHouseNumber, destination,
					houseNumber);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		String userURL = request.getRequestURL().toString();
		session.setAttribute(AttributeName.USER_URL, userURL);
		request.getSession().setAttribute(AttributeName.LAST_PAGE, PageName.CONFIRM_ORDER);
		return PageName.CONFIRM_ORDER;
	}

	private String createErrorMessage(String errorType, String userLocale) {
		Locale locale;
		if (userLocale == null) {
			locale = Locale.getDefault();
		} else {
			locale = new Locale(userLocale);
		}
		ResourceBundle bundle = ResourceBundle.getBundle(LOCALE_PROPERTIES, locale);
		String errorMessage = bundle.getString(errorType);
		return errorMessage;
	}
}
