package com.epam.itaxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.ClientService;
import com.epam.itaxi.validation.Validation;

public class ReplenishBalanceCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int replenishSum = Integer.parseInt(request.getParameter(AttributeName.REPLENISH_SUM));
		int clientId = (int) session.getAttribute(AttributeName.USER_ID);
		String cardNumber = request.getParameter(AttributeName.CARD_NUMBER);
		if (!Validation.getInstance().validateCardNum(cardNumber)) {
			request.setAttribute(AttributeName.INVALID_DATA, true);
			return PageName.AUTHORIZATION;
		}
		try {
			if (ClientService.getInstance().replenishBalance(clientId, replenishSum, cardNumber)) {
				int currentBalance = (int) session.getAttribute(AttributeName.CLIENT_BALANCE);
				session.setAttribute(AttributeName.CLIENT_BALANCE, currentBalance + replenishSum);
				request.setAttribute(AttributeName.SUCCESS_OPERATION, true);
			} else {
				request.setAttribute(AttributeName.INVALID_DATA, true);
			}
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		session.setAttribute(AttributeName.LAST_PAGE, PageName.CLIENT_PROFILE);
		return PageName.CLIENT_PROFILE;
	}
}
