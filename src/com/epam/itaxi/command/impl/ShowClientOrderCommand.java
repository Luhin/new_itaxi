package com.epam.itaxi.command.impl;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.entity.Order;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.OrderService;

public class ShowClientOrderCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int userId = (int) session.getAttribute(AttributeName.USER_ID);
		ArrayList<Order> orderList;
		try {
			orderList = OrderService.getInstance().findClientOrder(userId);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		session.setAttribute(AttributeName.ORDER_LIST, orderList);
		session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_ORDER);
		return PageName.USER_ORDER;
	}
}
