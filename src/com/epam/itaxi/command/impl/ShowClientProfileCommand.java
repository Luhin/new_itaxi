package com.epam.itaxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.ClientService;
import com.epam.itaxi.service.impl.OrderService;
import com.epam.itaxi.service.impl.UserService;

public class ShowClientProfileCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int clientId = (int) session.getAttribute(AttributeName.USER_ID);
		String clientRating;
		int clientBalance;
		int rideQuantity;
		try {
			clientRating = UserService.getInstance().findUserRating(clientId);
			clientBalance = ClientService.getInstance().checkClientBalance(clientId);
			rideQuantity = OrderService.getInstance().findClientRideQuantity(clientId);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		session.setAttribute(AttributeName.USER_RATING, clientRating);
		session.setAttribute(AttributeName.CLIENT_BALANCE, clientBalance);
		session.setAttribute(AttributeName.RIDE_QUANTITY, rideQuantity);
		session.setAttribute(AttributeName.LAST_PAGE, PageName.CLIENT_PROFILE);
		return PageName.CLIENT_PROFILE;
	}
}
