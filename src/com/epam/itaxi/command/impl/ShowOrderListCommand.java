package com.epam.itaxi.command.impl;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.entity.Order;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.service.impl.OrderService;

public class ShowOrderListCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		ArrayList<Order> orderList = OrderService.getInstance().showOrders();
		HttpSession session = request.getSession();
		session.setAttribute(AttributeName.ORDER_LIST, orderList);
		session.setAttribute(AttributeName.LAST_PAGE, PageName.ORDER_LIST);
		return PageName.ORDER_LIST;
	}
}
