package com.epam.itaxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.OrderService;

public class LogoutCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int userId = (int) session.getAttribute(AttributeName.USER_ID);
		try {
			OrderService.getInstance().deleteOrder(userId);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		session.invalidate();
		request.getSession().setAttribute(AttributeName.LAST_PAGE, PageName.AUTHORIZATION);
		return PageName.AUTHORIZATION;
	}
}
