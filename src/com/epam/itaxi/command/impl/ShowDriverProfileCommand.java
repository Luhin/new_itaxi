package com.epam.itaxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.DriverService;
import com.epam.itaxi.service.impl.OrderService;
import com.epam.itaxi.service.impl.UserService;

public class ShowDriverProfileCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int driverId = (int) session.getAttribute(AttributeName.USER_ID);
		String driverRating;
		String driverCar;
		int orderQuantity;
		try {
			driverRating = UserService.getInstance().findUserRating(driverId);
			driverCar = DriverService.getInstance().findDriverCar(driverId);
			orderQuantity = OrderService.getInstance().findDriverOrderQuantity(driverId);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		session.setAttribute(AttributeName.USER_RATING, driverRating);
		session.setAttribute(AttributeName.DRIVER_CAR, driverCar);
		session.setAttribute(AttributeName.RIDE_QUANTITY, orderQuantity);
		session.setAttribute(AttributeName.LAST_PAGE, PageName.DRIVER_PROFILE);
		return PageName.DRIVER_PROFILE;
	}
}
