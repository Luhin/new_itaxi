package com.epam.itaxi.command.impl;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.entity.Order;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.OrderService;

public class AcceptOrderCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int driverId = (int) session.getAttribute(AttributeName.USER_ID);
		int clientId = Integer.parseInt(request.getParameter(AttributeName.CLIENT_ID));
		session.setAttribute(AttributeName.ORDER_ID, clientId);
		try {
			if (OrderService.getInstance().acceptOrder(clientId, driverId)) {
				String userURL = request.getRequestURL().toString();
				session.setAttribute(AttributeName.USER_URL, userURL);
				session.setAttribute(AttributeName.LAST_PAGE, PageName.RESULT_ORDER);
				return PageName.RESULT_ORDER;
			} else {
				ArrayList<Order> orderList = OrderService.getInstance().showOrders();
				session.setAttribute(AttributeName.ORDER_LIST, orderList);
				session.setAttribute(AttributeName.LAST_PAGE, PageName.ORDER_LIST);
				request.setAttribute(AttributeName.INVALID_DATA, true);
				return PageName.ORDER_LIST;
			}
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
	}
}
