package com.epam.itaxi.command.impl;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.entity.UserRole;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.OrderService;
import com.epam.itaxi.validation.Validation;

public class EditCommentCommand implements ICommand {
	private final static String LOCALE_PROPERTIES = "property.locale";
	private final static String INCORRECT_COMMENT = "locale.error.incorrect.comment";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int orderId = (int) session.getAttribute(AttributeName.ORDER_ID);
		String newComment = request.getParameter(AttributeName.NEW_COMMENT);
		String userLocale = (String) session.getAttribute(AttributeName.LOCALE);
		if (!Validation.getInstance().validateComment(newComment)) {
			String errorMessage = createErrorMessage(INCORRECT_COMMENT, userLocale);
			request.setAttribute(AttributeName.INVALID_REGISTR_DATA, errorMessage);
			return PageName.AUTHORIZATION;
		}
		UserRole userRole = UserRole.valueOf(session.getAttribute(AttributeName.USER_ROLE).toString().toUpperCase());
		try {
			if (userRole == UserRole.CLIENT) {
				OrderService.getInstance().editClientComment(orderId, userRole, newComment);
				session.setAttribute(AttributeName.LAST_PAGE, PageName.CLIENT_PROFILE);
				request.setAttribute(AttributeName.SUCCESS_OPERATION, true);
				return PageName.CLIENT_PROFILE;
			} else {
				OrderService.getInstance().editDriverComment(orderId, userRole, newComment);
				session.setAttribute(AttributeName.LAST_PAGE, PageName.DRIVER_PROFILE);
				request.setAttribute(AttributeName.SUCCESS_OPERATION, true);
				return PageName.DRIVER_PROFILE;
			}
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
	}

	private String createErrorMessage(String errorType, String userLocale) {
		Locale locale;
		if (userLocale == null) {
			locale = Locale.getDefault();
		} else {
			locale = new Locale(userLocale);
		}
		ResourceBundle bundle = ResourceBundle.getBundle(LOCALE_PROPERTIES, locale);
		String errorMessage = bundle.getString(errorType);
		return errorMessage;
	}
}
