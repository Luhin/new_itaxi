package com.epam.itaxi.command.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.OrderService;

public class WaitOrderAcceptanceCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int clientId = (int) session.getAttribute(AttributeName.USER_ID);
		String answer;
		try {
			answer = OrderService.getInstance().waitOrderAcceptance(clientId);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		try {
			response.getWriter().write(answer);
		} catch (IOException e1) {
			throw new CommandException(e1);
		}
		return PageName.AJAX;
	}
}