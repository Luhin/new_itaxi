package com.epam.itaxi.command.impl;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.entity.UserRole;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.OrderService;
import com.epam.itaxi.validation.Validation;

public class RateOrderCommand implements ICommand {
	private final static String LOCALE_PROPERTIES = "property.locale";
	private final static String INCORRECT_COMMENT = "locale.error.incorrect.comment";
	private final static String INCORRECT_RATING = "locale.error.incorrect.rating";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		String comment = request.getParameter(AttributeName.COMMENT);
		String rating = request.getParameter(AttributeName.RATING);
		String userLocale = (String) session.getAttribute(AttributeName.LOCALE);
		String errorMessage = validateData(comment, rating, userLocale);
		if (errorMessage != null) {
			request.setAttribute(AttributeName.INVALID_DATA, errorMessage);
			return PageName.RESULT_ORDER;
		}
		UserRole userRole = UserRole.valueOf(session.getAttribute(AttributeName.USER_ROLE).toString().toUpperCase());
		int userId = (int) session.getAttribute(AttributeName.USER_ID);
		try {
			OrderService.getInstance().rateOrder(userRole, userId, comment, rating);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		request.setAttribute(AttributeName.SUCCESS_ORDER, true);
		session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_PAGE);
		return PageName.USER_PAGE;
	}

	private String validateData(String comment, String rating, String userLocale) {
		Validation validation = Validation.getInstance();
		if (!validation.validateComment(comment)) {
			return createErrorMessage(INCORRECT_COMMENT, userLocale);
		}
		if (!validation.validateRating(rating)) {
			return createErrorMessage(INCORRECT_RATING, userLocale);
		} else {
			return null;
		}
	}

	private String createErrorMessage(String errorType, String userLocale) {
		Locale locale;
		if (userLocale == null) {
			locale = Locale.getDefault();
		} else {
			locale = new Locale(userLocale);
		}
		ResourceBundle bundle = ResourceBundle.getBundle(LOCALE_PROPERTIES, locale);
		String errorMessage = bundle.getString(errorType);
		return errorMessage;
	}
}
