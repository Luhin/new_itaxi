package com.epam.itaxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.DriverService;
import com.epam.itaxi.validation.Validation;

public class ChangeDriverCarCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int driverId = (int) session.getAttribute(AttributeName.USER_ID);
		String newCar = request.getParameter(AttributeName.NEW_CAR);
		String password = request.getParameter(AttributeName.PASSWORD);
		if (!Validation.getInstance().validatePassword(password)) {
			request.setAttribute(AttributeName.INVALID_DATA, true);
			return PageName.AUTHORIZATION;
		}
		try {
			if (DriverService.getInstance().changeDriverCar(driverId, newCar, password)) {
				session.setAttribute(AttributeName.DRIVER_CAR, newCar);
				request.setAttribute(AttributeName.SUCCESS_OPERATION, true);
			} else {
				request.setAttribute(AttributeName.INVALID_DATA, true);
			}
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		session.setAttribute(AttributeName.LAST_PAGE, PageName.DRIVER_PROFILE);
		return PageName.DRIVER_PROFILE;
	}
}
