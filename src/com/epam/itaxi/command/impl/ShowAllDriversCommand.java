package com.epam.itaxi.command.impl;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.DriverService;

public class ShowAllDriversCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		ArrayList<User> driverList;
		try {
			driverList = DriverService.getInstance().findAllDrivers();
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		session.setAttribute(AttributeName.USER_LIST, driverList);
		session.setAttribute(AttributeName.LAST_PAGE, PageName.SHOW_ALL_DRIVERS);
		return PageName.SHOW_ALL_DRIVERS;
	}
}
