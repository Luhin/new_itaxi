package com.epam.itaxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.exception.CommandException;

public class EditingCommentCommand implements ICommand{

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int orderId = Integer.parseInt(request.getParameter(AttributeName.ORDER_ID));
		session.setAttribute(AttributeName.ORDER_ID, orderId);
		session.setAttribute(AttributeName.LAST_PAGE, PageName.EDIT_COMMENT);
		return PageName.EDIT_COMMENT;
	}
}
