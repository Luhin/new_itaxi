package com.epam.itaxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.exception.CommandException;

public class LocalizationCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		String locale = request.getParameter(AttributeName.LOCALE);
		session.setAttribute(AttributeName.LOCALE, locale);
		String lastPage = (String)session.getAttribute(AttributeName.LAST_PAGE);
		if (lastPage == null) {
			return PageName.AUTHORIZATION;
		} else {
			return lastPage;
		}
	}
}
