package com.epam.itaxi.command.impl;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.UserService;

public class UnbanUserCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int userId = Integer.parseInt(request.getParameter(AttributeName.USER_ID));
		@SuppressWarnings("unchecked")
		ArrayList<User> userList = (ArrayList<User>) session.getAttribute(AttributeName.USER_LIST);
		try {
			UserService.getInstance().unBanUser(userId, userList);
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
		session.setAttribute(AttributeName.USER_LIST, userList);
		String page = (String) session.getAttribute(AttributeName.LAST_PAGE);
		return page;
	}
}
