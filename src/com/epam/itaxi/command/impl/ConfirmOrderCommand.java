package com.epam.itaxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.command.ICommand;
import com.epam.itaxi.command.PageName;
import com.epam.itaxi.exception.CommandException;
import com.epam.itaxi.exception.ServiceException;
import com.epam.itaxi.service.impl.OrderService;

public class ConfirmOrderCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		HttpSession session = request.getSession();
		int clientId = (int) session.getAttribute(AttributeName.USER_ID);
		try {
			if (OrderService.getInstance().confirmOrder(clientId)) {
				session.setAttribute(AttributeName.LAST_PAGE, PageName.RATE_ORDER);
				return PageName.RATE_ORDER;
			} else {
				request.setAttribute(AttributeName.MAKE_ORDER_ERROR, true);
				session.setAttribute(AttributeName.LAST_PAGE, PageName.CREATE_ORDER);
				return PageName.CREATE_ORDER;
			}
		} catch (ServiceException e) {
			throw new CommandException(e);
		}
	}
}
