package com.epam.itaxi.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.itaxi.exception.CommandException;

public interface ICommand {
	String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
