package com.epam.itaxi.exception;

public class ProxyConnectionException extends Exception {
	private static final long serialVersionUID = 4316169144251930856L;

	public ProxyConnectionException() {
		super();
	}

	public ProxyConnectionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ProxyConnectionException(String arg0) {
		super(arg0);
	}

	public ProxyConnectionException(Throwable arg0) {
		super(arg0);
	}
}
