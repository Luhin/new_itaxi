package com.epam.itaxi.connection_pool;

import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.itaxi.exception.ConnectionPoolException;
import com.epam.itaxi.exception.ProxyConnectionException;

/**
 * Connection manager, which regulates connection quantity, and connection life
 * cycle.
 * 
 * @author Uladzimir Luhin
 *
 */
public class ConnectionPool {
	/**
	 * Instance of a singleton ConnectionPool
	 */
	private static ConnectionPool instance;
	/**
	 * @see java.util.concurrent.atomic.AtomicBoolean
	 */
	private static AtomicBoolean isCreated = new AtomicBoolean(false);
	/**
	 * @see java.util.concurrent.locks.ReentrantLock
	 */
	private static ReentrantLock lock = new ReentrantLock();
	/**
	 * @see org.apache.logging.log4j.Logger
	 */
	private static Logger logger = LogManager.getLogger();
	/**
	 * Container, which will keep the connections
	 */
	private ArrayBlockingQueue<ProxyConnection> connections;
	/**
	 * Size of the pool.
	 */
	private final int POOL_SIZE;

	/**
	 * Private constructor, which creates and fills the pool by connections.
	 */
	private ConnectionPool() {
		try {
			ConnectorDB connectorDB = new ConnectorDB();
			POOL_SIZE = connectorDB.getPoolsize();
			connections = new ArrayBlockingQueue<>(POOL_SIZE);
			for (int i = 0; i < POOL_SIZE; i++) {
				connections.add(connectorDB.getConnection());
			}
		} catch (ProxyConnectionException | MissingResourceException e) {
			logger.fatal("Connection isn't initialized", e);
			throw new RuntimeException();
		}
	}

	public static ConnectionPool getInstance() {
		if (!isCreated.get()) {
			try {
				lock.lock();
				if (instance == null) {
					instance = new ConnectionPool();
					isCreated.getAndSet(true);
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}

	/**
	 * One connection from pool will be given.
	 * 
	 * @throws ConnectionPoolException
	 * 
	 * @return ProxyConnection
	 */
	public ProxyConnection takeConnection() throws ConnectionPoolException {
		ProxyConnection connection = null;
		try {
			connection = connections.take();
		} catch (InterruptedException e) {
			throw new ConnectionPoolException("Take connection fault", e);
		}
		return connection;
	}

	/**
	 * Check auto-commit, if false - call Connection.commit, then set
	 * auto-commit to true. Next return connection to the pool
	 * 
	 * @param connection
	 *            - ProxyConnection, which must be returned to the pool
	 * @throws ConnectionPoolException
	 */
	public void giveBackConnection(ProxyConnection connection) throws ConnectionPoolException {
		try {
			if (!connection.getAutoCommit()) {
				connection.commit();
				connection.setAutoCommit(true);
			}
			connections.put(connection);
		} catch (SQLException | InterruptedException e) {
			throw new ConnectionPoolException("Give back connection fault", e);
		}
	}

	/**
	 * Close connection one by one from the pool. Recommend to call this method,
	 * only if application will be stopped.
	 * 
	 * @throws ProxyConnectionException
	 * @throws ConnectionPoolException
	 */
	public void clearConnectionPool() throws ProxyConnectionException, ConnectionPoolException {
		for (int i = 0; i < POOL_SIZE; i++) {
			ProxyConnection connection = takeConnection();
			connection.reallyClose();
		}
	}
}
