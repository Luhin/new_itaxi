package com.epam.itaxi.connection_pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.epam.itaxi.exception.ProxyConnectionException;

/**
 * Creates the connection, takes database data and number of connections from
 * property file.
 * 
 * @author ��������
 *
 */
class ConnectorDB {
	/**
	 * Path to the properties file with database data
	 */
	private final static String DATABASE_PROP = "property.database";
	/**
	 * Key to database URL
	 */
	private final static String URI = "db.url";
	/**
	 * Key to database user name
	 */
	private final static String USER = "db.user";
	/**
	 * Key to database password
	 */
	private final static String PASSWORD = "db.password";
	/**
	 * Key to the number of connections, which must be created.
	 */
	private final static String POOL_SIZE = "db.poolsize";
	/**
	 * Total naumber of connections
	 */
	private static int poolsize;
	/**
	 * @see java.util.ResourceBundle
	 */
	private static ResourceBundle resource;

	ConnectorDB() {
		resource = ResourceBundle.getBundle(DATABASE_PROP);
		poolsize = Integer.parseInt(resource.getString(POOL_SIZE));
	}

	/**
	 * Creates the connection and wrap it in
	 * com.epam.itaxi.connection_pool.ProxyConnection.
	 * 
	 * @return If success, returns ProxyConnection.
	 * @throws ProxyConnectionException
	 */
	ProxyConnection getConnection() throws ProxyConnectionException {

		String url = resource.getString(URI);
		String user = resource.getString(USER);
		String pass = resource.getString(PASSWORD);
		Connection connection = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(url, user, pass);
		} catch (SQLException | ClassNotFoundException e) {
			throw new ProxyConnectionException(e);
		}
		ProxyConnection proxyConnection = new ProxyConnection(connection);
		return proxyConnection;
	}

	int getPoolsize() {
		return poolsize;
	}
}