package com.epam.itaxi.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.entity.User;

/**
 * Implements javax.servlet.jsp.tagext.TagSupport. Creates a tag, which takes as
 * a parameter List with clients, and creates corresponding table
 * 
 * @see javax.servlet.jsp.tagext.TagSupport
 * 
 * @author Uladzimir Luhin
 *
 */
public class ShowAllClient extends TagSupport {
	private static final long serialVersionUID = -450464079239593285L;
	private final static String LOCALE_PROPERTIES = "property.locale";
	private ArrayList<User> clientList;
	private ResourceBundle bundle;

	public void setClientList(ArrayList<User> list) {
		this.clientList = list;
	}

	@Override
	public int doStartTag() throws JspException {
		String userLocale = (String) pageContext.getSession().getAttribute(AttributeName.LOCALE);
		Locale locale;
		if (userLocale == null) {
			locale = Locale.getDefault();
		} else {
			locale = new Locale(userLocale);
		}
		bundle = ResourceBundle.getBundle(LOCALE_PROPERTIES, locale);
		String noClients = bundle.getString("locale.messagge.no.clients");
		String clientName = bundle.getString("locale.message.client.name");
		String clientRating = bundle.getString("locale.message.client.rating");
		String eMail = bundle.getString("locale.message.email");
		String banned = bundle.getString("locale.message.banned");
		String notBanned = bundle.getString("locale.message.not.banned");
		String ban = bundle.getString("locale.button.ban");
		String unban = bundle.getString("locale.button.unban");
		String status = bundle.getString("locale.message.status");
		String action = bundle.getString("locale.message.action");
		int pageUnique = (int) pageContext.getSession().getAttribute(AttributeName.PAGE_UNIQUE);
		try {
			JspWriter out = pageContext.getOut();
			if (clientList == null || clientList.isEmpty()) {
				out.write(noClients);
			} else {
				out.write("<table class='table table-bordered table-condensed' style='width: 60%'><tr><th>" + clientName
						+ "</th><th>" + clientRating + "</th><th>" + eMail + "</th><th>" + status + "</th><th>" + action
						+ "</th></tr>");
				for (User user : clientList) {
					out.write("<tr><td>");
					out.write(user.getName());
					out.write("</td><td>");
					out.write(user.getRating());
					out.write("</td><td>");
					out.write(user.getEMail());
					out.write("</td><td>");
					if (user.isBanned()) {
						out.write(banned);
						out.write("</td><td>");
						out.write(
								"<form action='Controller' method='post'><input type='hidden' name='command' value='unban_user' /><input type='hidden' name='pageUnique' value='"
										+ pageUnique + "' /><input type='hidden' name='userId' value='"
										+ user.getUserId() + "'> <input class='btn btn-info' type='submit' value='"
										+ unban + "' /></form>");
						out.write("</td></tr>");
					} else {
						out.write(notBanned);
						out.write("</td><td>");
						out.write(
								"<form action='Controller' method='post'><input type='hidden' name='command' value='ban_user' /><input type='hidden' name='pageUnique' value='"
										+ pageUnique + "' /><input type='hidden' name='userId' value='"
										+ user.getUserId() + "'> <input class='btn btn-primary' type='submit' value='"
										+ ban + "' /></form>");
						out.write("</td></tr>");
					}
				}
				out.write("</table>");
			}
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}
}
