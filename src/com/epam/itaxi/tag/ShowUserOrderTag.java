package com.epam.itaxi.tag;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.epam.itaxi.command.AttributeName;
import com.epam.itaxi.entity.Order;
import com.epam.itaxi.entity.UserRole;

/**
 * Implements javax.servlet.jsp.tagext.TagSupport. Creates a tag, which takes as
 * a parameter List with user orders, and creates corresponding table
 * 
 * @see javax.servlet.jsp.tagext.TagSupport
 * 
 * @author Uladzimir Luhin
 *
 */
public class ShowUserOrderTag extends TagSupport {
	private static final long serialVersionUID = -4235625769575067233L;
	private final static String DATE_FORMAT = "yyyy-mm-dd hh:mm:ss";
	private final static String MONEY_FORMAT = "00.00 BYN";
	private ArrayList<Order> orderList;
	private ResourceBundle bundle;

	public void setOrderList(ArrayList<Order> list) {
		this.orderList = list;
	}

	@Override
	public int doStartTag() throws JspException {
		String userLocale = (String) pageContext.getSession().getAttribute(AttributeName.LOCALE);
		Locale locale;
		if (userLocale == null) {
			locale = Locale.getDefault();
		} else {
			locale = new Locale(userLocale);
		}
		bundle = ResourceBundle.getBundle("property.locale", locale);
		String noRides = bundle.getString("locale.message.no.user.order");
		String date = bundle.getString("locale.message.ride.date");
		String position = bundle.getString("locale.destination.from");
		String destination = bundle.getString("locale.destination.to");
		String subjectName = null;
		String driverComment = null;
		String clientComment = null;
		if (UserRole.valueOf(((String) pageContext.getSession().getAttribute(AttributeName.USER_ROLE))
				.toUpperCase()) == UserRole.CLIENT) {
			subjectName = bundle.getString("locale.message.driver.name");
			driverComment = bundle.getString("locale.message.driver.comment");
			clientComment = bundle.getString("locale.message.my.comment");
		} else {
			subjectName = bundle.getString("locale.message.client.name");
			driverComment = bundle.getString("locale.message.my.comment");
			clientComment = bundle.getString("locale.message.client.comment");
		}
		String price = bundle.getString("locale.message.price");
		String action = bundle.getString("locale.message.action");
		String editMyComment = bundle.getString("locale.button.edit.comment");
		try {
			JspWriter out = pageContext.getOut();
			if (orderList == null || orderList.isEmpty()) {
				out.write(noRides);
			} else {
				out.write("<table class='table table-bordered table-condensed'><tr><th>" + date + "</th><th>" + position
						+ "</th><th>" + destination + "</th><th>" + subjectName + "</th><th>" + price + "</th><th>"
						+ driverComment + "</th>" + "<th>" + clientComment + "</th><th>" + action + "</th></tr>");
				for (Order order : orderList) {
					out.write("<tr><td>");
					SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
					out.write(sdf.format(order.getDate().getTime()));
					out.write("</td><td>");
					out.write(order.getPosition());
					out.write(" ");
					out.write(Integer.toString(order.getPositionHouse()));
					out.write("</td><td>");
					out.write(order.getDestination());
					out.write(" ");
					out.write(Integer.toString(order.getDestinationHouse()));
					out.write("</td><td>");
					out.write(order.getDriverName());
					out.write("</td><td>");
					DecimalFormat decimalFormat = new DecimalFormat(MONEY_FORMAT);
					String formattedPrice = decimalFormat.format(order.getPrice());
					out.write(formattedPrice);
					out.write("</td><td>");
					if (order.getDriverComment() != null) {
						out.write(order.getDriverComment());
					}
					out.write("</td><td>");
					if (order.getClientComment() != null) {
						out.write(order.getClientComment());
					}
					out.write("</td><td>");
					out.write(
							"<form action='Controller' method='post'><input type='hidden' name='command' value='editing' /><input type='hidden' name='orderId' value='"
									+ order.getOrderId() + "'> <input class='btn' type='submit' value='" + editMyComment
									+ "' /></form>");
					out.write("</td></tr>");
				}
				out.write("</table>");
			}
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}
}
