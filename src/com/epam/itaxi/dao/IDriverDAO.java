package com.epam.itaxi.dao;

import java.util.List;

import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.DAOException;

/**
 * Connects to the database, and make some actions, relative to the driver.
 * 
 * @author Uladzimir Luhin
 *
 */
public interface IDriverDAO {
	/**
	 * Find current drivers car model.
	 * 
	 * @param driverId
	 *            ID of a driver, who car should be find
	 * @return String contains car model
	 * @throws DAOException
	 */
	String findDriverCarModel(int driverId) throws DAOException;

	/**
	 * Changes drivers car model to the new car model.
	 * 
	 * @param driverId
	 *            ID of a driver, who car should be changed
	 * @param newCar
	 *            new car model
	 * @throws DAOException
	 */
	void changeDriverCar(int driverId, String newCar) throws DAOException;

	/**
	 * Rate driver for the last order.
	 * 
	 * @param driverId
	 *            ID of a driver, who must be rated
	 * @param rating
	 * @throws DAOException
	 */
	void rateDriver(int driverId, int rating) throws DAOException;

	/**
	 * Add a new driver to the database.
	 * 
	 * @param name
	 *            driver name
	 * @param login
	 *            driver login
	 * @param password
	 *            driver password
	 * @param email
	 *            driver Email
	 * @param carModel
	 *            driver car model
	 * @throws DAOException
	 */
	void signUpDriver(String name, String login, String password, String email, String carModel) throws DAOException;

	/**
	 * Find all drivers from the database.
	 * 
	 * @return List with all drivers.
	 * @throws DAOException
	 */
	List<User> findAllDrivers() throws DAOException;
}
