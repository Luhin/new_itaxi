package com.epam.itaxi.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.itaxi.connection_pool.ConnectionPool;
import com.epam.itaxi.connection_pool.ProxyConnection;
import com.epam.itaxi.dao.IOrderDAO;
import com.epam.itaxi.entity.Order;
import com.epam.itaxi.exception.ConnectionPoolException;
import com.epam.itaxi.exception.DAOException;

public class OrderDAO implements IOrderDAO {
	private static Logger logger = LogManager.getLogger();
	private final static OrderDAO INSTANCE = new OrderDAO();
	private final static String CHOOSE_STREET = "select street from address";
	private final static String FIND_CLIENT_RIDE_COUNT = "select count(order_id) from ordering where client_id = ?";
	private final static String FIND_DRIVER_ORDER_COUNT = "select count(order_id) from ordering where driver_id = ?";
	private final static String FIND_CLIENT_ORDER = "select order_id, date, address_1.street, position_house, address_2.street, dest_house, user.name, price, driver_comment, client_comment from ordering join address as address_1 on ordering.position = address_1.address_id join address as address_2 on ordering.destination = address_2.address_id join user on ordering.driver_id = user.user_id where ordering.client_id = ?";
	private final static String EDIT_CLIENT_COMMENT = "update ordering set client_comment = ? where order_id = ?";
	private final static String EDIT_DRIVER_COMMENT = "update ordering set driver_comment = ? where order_id = ?";
	private final static String FIND_DRIVER_ORDER = "select order_id, date, address_1.street, position_house, address_2.street, dest_house, user.name, price, driver_comment, client_comment from ordering join address as address_1 on ordering.position = address_1.address_id join address as address_2 on ordering.destination = address_2.address_id join user on ordering.client_id = user.user_id where ordering.driver_id = ?";
	private final static String APPEND_ORDER = "insert into ordering (driver_id, client_id, date, price, position, position_house, destination, dest_house) values (?, ?, ?, ?, (select address_id from address where street = ?), ?,(select address_id from address where street = ?), ?)";
	private final static String TAKE_CLIENT_MONEY = "update client set money_amount = money_amount - ? where client_id = ?";
	private final static String GIVE_MONEY_ADMIN = "update client set money_amount = money_amount + ? where client_id = 1";
	private final static String FIND_LAST_DRIVER_ORDER = "select order_id, client_id from ordering where driver_id = ? order by order_id desc limit 1";
	private final static String FIND_LAST_CLIENT_ORDER = "select order_id, driver_id from ordering where client_id = ? order by order_id desc limit 1";

	private OrderDAO() {
	}

	public static OrderDAO getInstance() {
		return INSTANCE;
	}

	public ArrayList<String> makeStreetList() throws DAOException {
		ArrayList<String> streetList = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(CHOOSE_STREET);
				ResultSet resultSet = statement.executeQuery();) {
			while (resultSet.next()) {
				streetList.add(resultSet.getString(1));
			}
			return streetList;
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Make street list fault", e);
		}
	}

	public int findClientRideQuantity(int userId) throws DAOException {
		int driveCount = 0;
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_CLIENT_RIDE_COUNT);) {
			statement.setInt(1, userId);
			try (ResultSet resultSet = statement.executeQuery();) {
				if (resultSet.next()) {
					driveCount = resultSet.getInt(1);
				}
				return driveCount;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Find client ride fault", e);
		}
	}

	public int findDriverOrderQuantity(int userId) throws DAOException {
		int orderCount = 0;
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_DRIVER_ORDER_COUNT);) {
			statement.setInt(1, userId);
			try (ResultSet resultSet = statement.executeQuery();) {
				if (resultSet.next()) {
					orderCount = resultSet.getInt(1);
				}
				return orderCount;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Find driver ride fault", e);
		}
	}

	public ArrayList<Order> findDriverOrder(int userId) throws DAOException {
		ArrayList<Order> orderList = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_DRIVER_ORDER);) {
			statement.setInt(1, userId);
			try (ResultSet resultSet = statement.executeQuery();) {
				while (resultSet.next()) {
					Order order = new Order();
					order.setOrderId(resultSet.getInt(1));
					long date = resultSet.getLong(2);
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(date);
					order.setDate(calendar);
					order.setPosition(resultSet.getString(3));
					order.setPositionHouse(resultSet.getInt(4));
					order.setDestination(resultSet.getString(5));
					order.setDestinationHouse(resultSet.getInt(6));
					order.setDriverName(resultSet.getString(7));
					order.setPrice(resultSet.getDouble(8));
					order.setDriverComment(resultSet.getString(9));
					order.setClientComment(resultSet.getString(10));
					orderList.add(order);
				}
				return orderList;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Find driver order fault", e);
		}
	}

	public ArrayList<Order> findClientOrder(int userId) throws DAOException {
		ArrayList<Order> orderList = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_CLIENT_ORDER);) {
			statement.setInt(1, userId);
			try (ResultSet resultSet = statement.executeQuery();) {
				while (resultSet.next()) {
					Order order = new Order();
					order.setOrderId(resultSet.getInt(1));
					long date = resultSet.getLong(2);
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(date);
					order.setDate(calendar);
					order.setPosition(resultSet.getString(3));
					order.setPositionHouse(resultSet.getInt(4));
					order.setDestination(resultSet.getString(5));
					order.setDestinationHouse(resultSet.getInt(6));
					order.setDriverName(resultSet.getString(7));
					order.setPrice(resultSet.getInt(8));
					order.setDriverComment(resultSet.getString(9));
					order.setClientComment(resultSet.getString(10));
					orderList.add(order);
				}
				return orderList;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Find client order fault", e);
		}
	}

	public void editClientComment(int orderId, String newComment) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(EDIT_CLIENT_COMMENT);) {
			statement.setString(1, newComment);
			statement.setInt(2, orderId);
			statement.executeUpdate();
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Edit client comment fault", e);
		}
	}

	public void editDriverComment(int orderId, String newComment) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(EDIT_DRIVER_COMMENT);) {
			statement.setString(1, newComment);
			statement.setInt(2, orderId);
			statement.executeUpdate();
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Edit driver comment fault", e);
		}
	}

	public boolean appendOrder(Order order) throws DAOException {
		ProxyConnection connection = null;
		PreparedStatement appendOrder = null;
		PreparedStatement takeClientMoney = null;
		PreparedStatement giveMoneyAdmin = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			connection.setAutoCommit(false);
			appendOrder = connection.prepareStatement(APPEND_ORDER);
			appendOrder.setInt(1, order.getDriverId());
			appendOrder.setInt(2, order.getClientId());
			Calendar calendar = Calendar.getInstance();
			appendOrder.setLong(3, calendar.getTimeInMillis());
			appendOrder.setDouble(4, order.getPrice());
			appendOrder.setString(5, order.getPosition());
			appendOrder.setInt(6, order.getPositionHouse());
			appendOrder.setString(7, order.getDestination());
			appendOrder.setInt(8, order.getDestinationHouse());
			appendOrder.executeUpdate();
			takeClientMoney = connection.prepareStatement(TAKE_CLIENT_MONEY);
			takeClientMoney.setDouble(1, order.getPrice());
			takeClientMoney.setInt(2, order.getClientId());
			takeClientMoney.executeUpdate();
			giveMoneyAdmin = connection.prepareStatement(GIVE_MONEY_ADMIN);
			giveMoneyAdmin.setDouble(1, order.getPrice());
			giveMoneyAdmin.executeUpdate();
			connection.commit();
			connection.setAutoCommit(true);
			return true;
		} catch (SQLException | ConnectionPoolException e) {
			try {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
				}
			} catch (SQLException e1) {
				throw new DAOException("Sign up client fault", e1);
			}
			throw new DAOException("Sign up client fault", e);
		} finally {
			if (appendOrder != null) {
				try {
					appendOrder.close();
				} catch (SQLException e) {
					logger.error("Statement not closed", e);
				}
			}
			if (takeClientMoney != null) {
				try {
					takeClientMoney.close();
				} catch (SQLException e) {
					logger.error("Statement not closed", e);
				}
			}
			if (giveMoneyAdmin != null) {
				try {
					giveMoneyAdmin.close();
				} catch (SQLException e) {
					logger.error("Statement not closed", e);
				}
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public int addCommentClient(int driverId, String comment) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement findOrderId = connection.prepareStatement(FIND_LAST_DRIVER_ORDER);) {
			findOrderId.setInt(1, driverId);
			int orderId = 0;
			int clientId = 0;
			try (ResultSet resultSet = findOrderId.executeQuery();) {
				if (resultSet.next()) {
					orderId = resultSet.getInt(1);
					clientId = resultSet.getInt(2);
				}
			}
			try (PreparedStatement addComment = connection.prepareStatement(EDIT_DRIVER_COMMENT)) {
				addComment.setString(1, comment);
				addComment.setInt(2, orderId);
				addComment.executeUpdate();
			}
			return clientId;
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Comment not added", e);
		}
	}

	public int addCommentDriver(int clientId, String comment) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement findOrderId = connection.prepareStatement(FIND_LAST_CLIENT_ORDER);) {
			findOrderId.setInt(1, clientId);
			int orderId = 0;
			int driveId = 0;
			try (ResultSet resultSet = findOrderId.executeQuery();) {
				if (resultSet.next()) {
					orderId = resultSet.getInt(1);
					driveId = resultSet.getInt(2);
				}
			}
			try (PreparedStatement addComment = connection.prepareStatement(EDIT_CLIENT_COMMENT)) {
				addComment.setString(1, comment);
				addComment.setInt(2, orderId);
				addComment.executeUpdate();
			}
			return driveId;
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Comment not added", e);
		}
	}
}
