package com.epam.itaxi.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.epam.itaxi.connection_pool.ConnectionPool;
import com.epam.itaxi.connection_pool.ProxyConnection;
import com.epam.itaxi.dao.IUserDAO;
import com.epam.itaxi.entity.User;
import com.epam.itaxi.entity.UserRole;
import com.epam.itaxi.exception.ConnectionPoolException;
import com.epam.itaxi.exception.DAOException;

public class UserDAO implements IUserDAO {
	private final static UserDAO INSTANCE = new UserDAO();
	private final static String FIND_USER = "select user_id, role, login, password, name from user where login=?";
	private final static String FIND_USER_NAME = "select name from user where user_id = ?";
	private final static String FIND_USER_RATING = "select  mark_sum / mark_count from rating where user_id = ?";
	private final static String FIND_USER_PASSWORD = "select password from user where user_id = ?";
	private final static String CHECK_MATCH_LOGIN = "select user_id from user where login = ?";
	private final static String IS_BANNED = "select banned from user where user_id = ?";
	private final static String UNBAN_USER = "update user set banned = 0 where user_id = ?";
	private final static String BAN_USER = "update user set banned = 1 where user_id = ?";

	private UserDAO() {
	}

	public static UserDAO getInstance() {
		return INSTANCE;
	}

	public User authorizeUser(String login, String password) throws DAOException {
		User user = null;

		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_USER);) {
			statement.setString(1, login);
			try (ResultSet resultSet = statement.executeQuery();) {
				if (resultSet.next()) {
					String dblogin = resultSet.getString(3);
					String dbPassword = resultSet.getString(4);
					if (login.equals(dblogin) && password.equals(dbPassword)) {
						user = new User();
						int userId = Integer.parseInt(resultSet.getString(1));
						String role = resultSet.getString(2);
						String name = resultSet.getString(5);
						user.setUserId(userId);
						user.setName(name);
						user.setRole(UserRole.valueOf(role.toUpperCase()));
					}
				}
				return user;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Authorization fault", e);
		}
	}

	public String findUserName(int userId) throws DAOException {
		String userName = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_USER_NAME);) {
			statement.setInt(1, userId);
			try (ResultSet resultSet = statement.executeQuery();) {
				if (resultSet.next()) {
					userName = resultSet.getString(1);
				}
				return userName;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Find user name fault", e);
		}
	}

	public boolean checkMatchExistLogin(String login) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(CHECK_MATCH_LOGIN);) {
			statement.setString(1, login);
			try (ResultSet resultSet = statement.executeQuery();) {
				return resultSet.next();
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Login match fault", e);
		}
	}

	public boolean chekUserPassword(int userId, String password) throws DAOException {
		String dbPassword = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_USER_PASSWORD);) {
			statement.setInt(1, userId);
			try (ResultSet resultSet = statement.executeQuery();) {
				if (resultSet.next()) {
					dbPassword = resultSet.getString(1);
				}
				return password.equals(dbPassword);
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Check password fault", e);
		}
	}

	public double findUserRating(int userId) throws DAOException {
		double userRating = 0;
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_USER_RATING);) {
			statement.setInt(1, userId);
			try (ResultSet resultSet = statement.executeQuery();) {
				if (resultSet.next()) {
					userRating = resultSet.getDouble(1);
				}
				return userRating;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Find user rating fault", e);
		}
	}

	public boolean isBanned(int userId) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(IS_BANNED);) {
			statement.setInt(1, userId);
			try (ResultSet resultSet = statement.executeQuery()) {
				boolean answer = false;
				if (resultSet.next()) {
					return resultSet.getBoolean(1);
				}
				return answer;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Is banned check failure", e);
		}
	}

	public void unBanUser(int userId) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(UNBAN_USER);) {
			statement.setInt(1, userId);
			statement.executeUpdate();
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Unban user failure", e);
		}
	}

	public void banUser(int userId) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(BAN_USER);) {
			statement.setInt(1, userId);
			statement.executeUpdate();
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Ban user failure", e);
		}
	}
}
