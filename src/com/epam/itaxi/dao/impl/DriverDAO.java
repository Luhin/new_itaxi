package com.epam.itaxi.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.epam.itaxi.connection_pool.ConnectionPool;
import com.epam.itaxi.connection_pool.ProxyConnection;
import com.epam.itaxi.dao.IDriverDAO;
import com.epam.itaxi.entity.User;
import com.epam.itaxi.entity.UserRole;
import com.epam.itaxi.exception.ConnectionPoolException;
import com.epam.itaxi.exception.DAOException;

public class DriverDAO implements IDriverDAO {
	private final static DriverDAO INSTANCE = new DriverDAO();
	private final static String FIND_CAR_MODEL = "select car_model from driver where driver_id = ?";
	private final static String CHANGE_DRIVER_CAR = "update driver set car_model = ? where driver_id = ?";
	private final static String RATE_DRIVER = "update rating set mark_sum = mark_sum + ?, mark_count = mark_count + 1 where user_id = ?";
	private final static String ADD_USER_DATA = "insert into user (role, login, password, name, e_mail, banned) values(?, ?, ?, ?, ?, ?)";
	private final static String FIND_ID_BY_LOGIN = "select user_id from user where login = ?";
	private final static String ADD_START_RATING = "insert into rating values(?, ?, 1)";
	private final static String ADD_DRIVER_DATA = "insert into driver (driver_id, car_model) values (?, ?)";
	private final static int START_RATING = 4;
	private final static String FIND_ALL_DRIVERS = "select user.user_id, user.name, user.e_mail, user.banned, driver.car_model, mark_sum / mark_count  from user join driver on user.user_id = driver.driver_id join rating on user.user_id = rating.user_id where role='driver'";

	private DriverDAO() {
	}

	public static DriverDAO getInstance() {
		return INSTANCE;
	}

	public String findDriverCarModel(int driverId) throws DAOException {
		String carModel = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_CAR_MODEL);) {
			statement.setInt(1, driverId);
			try (ResultSet resultSet = statement.executeQuery();) {
				if (resultSet.next()) {
					carModel = resultSet.getString(1);
				}
				return carModel;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Find driver car fault", e);
		}
	}

	public void changeDriverCar(int driverId, String newCar) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(CHANGE_DRIVER_CAR);) {
			statement.setString(1, newCar);
			statement.setInt(2, driverId);
			statement.executeUpdate();
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Change driver car fault", e);
		}
	}

	public void rateDriver(int driverId, int rating) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(RATE_DRIVER);) {
			statement.setInt(1, rating);
			statement.setInt(2, driverId);
			statement.executeUpdate();
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Determinate client position fault", e);
		}
	}

	public void signUpDriver(String name, String login, String password, String email, String carModel)
			throws DAOException {
		int driverId = 0;
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();) {
			connection.setAutoCommit(false);
			try (PreparedStatement addUserData = connection.prepareStatement(ADD_USER_DATA);) {
				addUserData.setString(1, UserRole.DRIVER.toString().toLowerCase());
				addUserData.setString(2, login);
				addUserData.setString(3, password);
				addUserData.setString(4, name);
				addUserData.setString(5, email);
				addUserData.setBoolean(6, false);
				addUserData.executeUpdate();
			} catch (SQLException e) {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DAOException("Sign up driver fault", e);
				}
			}
			try (PreparedStatement findDriverId = connection.prepareStatement(FIND_ID_BY_LOGIN);) {
				findDriverId.setString(1, login);
				try (ResultSet resultSet = findDriverId.executeQuery();) {
					if (resultSet.next()) {
						driverId = resultSet.getInt(1);
					}
				}
			} catch (SQLException e) {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DAOException("Sign up driver fault", e);
				}
			}
			try (PreparedStatement addClientData = connection.prepareStatement(ADD_DRIVER_DATA);) {
				addClientData.setInt(1, driverId);
				addClientData.setString(2, carModel);
				addClientData.executeUpdate();
			} catch (SQLException e) {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DAOException("Sign up driver fault", e);
				}
			}
			try (PreparedStatement addStartRating = connection.prepareStatement(ADD_START_RATING);) {
				addStartRating.setInt(1, driverId);
				addStartRating.setInt(2, START_RATING);
				addStartRating.executeUpdate();
				connection.commit();
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DAOException("Sign up driver fault", e);
				}
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Sign up driver fault", e);
		}
	}

	public ArrayList<User> findAllDrivers() throws DAOException {
		ArrayList<User> driverList = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_ALL_DRIVERS);
				ResultSet resultSet = statement.executeQuery();) {
			while (resultSet.next()) {
				User user = new User();
				user.setUserId(resultSet.getInt(1));
				user.setName(resultSet.getString(2));
				user.setEMail(resultSet.getString(3));
				user.setBanned(resultSet.getBoolean(4));
				user.setCarModel(resultSet.getString(5));
				String rating = Double.toString(resultSet.getDouble(6));
				user.setRating(rating.substring(0, 3));
				driverList.add(user);
			}
			return driverList;
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Change driver car fault", e);
		}
	}
}
