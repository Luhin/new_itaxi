package com.epam.itaxi.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.epam.itaxi.connection_pool.ConnectionPool;
import com.epam.itaxi.connection_pool.ProxyConnection;
import com.epam.itaxi.dao.IClientDAO;
import com.epam.itaxi.entity.User;
import com.epam.itaxi.entity.UserRole;
import com.epam.itaxi.exception.ConnectionPoolException;
import com.epam.itaxi.exception.DAOException;

public class ClientDAO implements IClientDAO {
	private final static ClientDAO INSTANCE = new ClientDAO();
	private final static String FIND_POSITION = "select street from address order by rand() limit 1";
	private final static String CHECK_CLIENT_BALANCE = "select money_amount from client where client_id = ?";
	private final static String CHECK_CARD_NUMBER = "select card_number from client where client_id = ?";
	private final static String REPLENISH_BALANCE = "update client set money_amount = money_amount + ? where client_id = ?";
	private final static String ADD_USER_DATA = "insert into user (role, login, password, name, e_mail, banned) values(?, ?, ?, ?, ?, ?)";
	private final static int START_RATING = 4;
	private final static int START_MONEY_AMOUNT = 0;
	private final static String FIND_ID_BY_LOGIN = "select user_id from user where login = ?";
	private final static String ADD_CLIENT_DATA = "insert into client (client_id, money_amount, card_number) values (?, ?, ?)";
	private final static String RATE_CLIENT = "update rating set mark_sum = mark_sum + ?, mark_count = mark_count + 1 where user_id = ?";
	private final static String ADD_START_RATING = "insert into rating values(?, ?, 1)";
	private final static String FIND_ALL_CLIENTS = "select user.user_id, user.name, user.e_mail, user.banned, mark_sum / mark_count  from user join rating on user.user_id = rating.user_id where role='client'";

	private ClientDAO() {
	}

	public static ClientDAO getInstance() {
		return INSTANCE;
	}

	public String determineClientPosition() throws DAOException {
		String clientPosition = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_POSITION);
				ResultSet resultSet = statement.executeQuery();) {
			if (resultSet.next()) {
				clientPosition = resultSet.getString(1);
			}
			return clientPosition;
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Determinate client position fault", e);
		}
	}

	public int checkClientBalance(int clientId) throws DAOException {
		int clientBalance = 0;
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(CHECK_CLIENT_BALANCE);) {
			statement.setInt(1, clientId);
			try (ResultSet resultSet = statement.executeQuery();) {
				if (resultSet.next()) {
					clientBalance = resultSet.getInt(1);
				}
				return clientBalance;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Check client balance fault", e);
		}
	}

	public boolean checkCardNumber(int clientId, int cardNumber) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(CHECK_CARD_NUMBER);) {
			statement.setInt(1, clientId);
			try (ResultSet resultSet = statement.executeQuery();) {
				int dbCardNumber = 0;
				if (resultSet.next()) {
					dbCardNumber = resultSet.getInt(1);
				}
				return cardNumber == dbCardNumber;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Check card number fault", e);
		}
	}

	public void replenishBalance(int clientId, int replenishSum) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(REPLENISH_BALANCE);) {
			statement.setInt(1, replenishSum);
			statement.setInt(2, clientId);
			statement.executeUpdate();
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Replenish balance fault", e);
		}
	}

	public int signUpClient(String name, String login, String password, String email, String cardNum)
			throws DAOException {
		int clientId = 0;
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();) {
			connection.setAutoCommit(false);
			try (PreparedStatement addUserData = connection.prepareStatement(ADD_USER_DATA);) {
				addUserData.setString(1, UserRole.CLIENT.toString().toLowerCase());
				addUserData.setString(2, login);
				addUserData.setString(3, password);
				addUserData.setString(4, name);
				addUserData.setString(5, email);
				addUserData.setBoolean(6, false);
				addUserData.executeUpdate();
			} catch (SQLException e) {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DAOException("Sign up client fault", e);
				}
			}
			try (PreparedStatement findClientId = connection.prepareStatement(FIND_ID_BY_LOGIN);) {
				findClientId.setString(1, login);
				try (ResultSet resultSet = findClientId.executeQuery();) {
					if (resultSet.next()) {
						clientId = resultSet.getInt(1);
					}
				}
			} catch (SQLException e) {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DAOException("Sign up client fault", e);
				}
			}
			try (PreparedStatement addClientData = connection.prepareStatement(ADD_CLIENT_DATA);) {
				addClientData.setInt(1, clientId);
				addClientData.setInt(2, START_MONEY_AMOUNT);
				addClientData.setInt(3, Integer.parseInt(cardNum));
				addClientData.executeUpdate();
			} catch (SQLException e) {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DAOException("Sign up client fault", e);
				}
			}
			try (PreparedStatement addStartRating = connection.prepareStatement(ADD_START_RATING);) {
				addStartRating.setInt(1, clientId);
				addStartRating.setInt(2, START_RATING);
				addStartRating.executeUpdate();
				connection.commit();
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DAOException("Sign up client fault", e);
				}
			}
			return clientId;
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Sign up client fault", e);
		}
	}

	public void rateClient(int clientId, int rating) throws DAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(RATE_CLIENT);) {
			statement.setInt(1, rating);
			statement.setInt(2, clientId);
			statement.executeUpdate();
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Determinate client position fault", e);
		}
	}

	public ArrayList<User> findAllClients() throws DAOException {
		ArrayList<User> clientList = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(FIND_ALL_CLIENTS);
				ResultSet resultSet = statement.executeQuery();) {
			while (resultSet.next()) {
				User user = new User();
				user.setUserId(resultSet.getInt(1));
				user.setName(resultSet.getString(2));
				user.setEMail(resultSet.getString(3));
				user.setBanned(resultSet.getBoolean(4));
				String rating = Double.toString(resultSet.getDouble(5));
				user.setRating(rating.substring(0, 3));
				clientList.add(user);
			}
			return clientList;
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException("Change driver car fault", e);
		}
	}
}
