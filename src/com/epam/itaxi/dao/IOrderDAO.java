package com.epam.itaxi.dao;

import java.util.List;

import com.epam.itaxi.entity.Order;
import com.epam.itaxi.exception.DAOException;

/**
 * Connects to the database, and make some actions, relative to the making
 * order.
 * 
 * @author Uladzimir Luhin
 *
 */
public interface IOrderDAO {
	/**
	 * Creates a list, contains possible addresse to go.
	 * 
	 * @return list, contains streets name
	 * @throws DAOException
	 */
	List<String> makeStreetList() throws DAOException;

	/**
	 * Find total quantity rides of a client.
	 * 
	 * @param userId
	 *            ID of a client, who rides must be calculate
	 * @return total number of trips
	 * @throws DAOException
	 */
	int findClientRideQuantity(int userId) throws DAOException;

	/**
	 * Find total quantity rides of a driver.
	 * 
	 * @param userId
	 *            ID of a driver, who rides must be calculate
	 * @return total number of trips
	 * @throws DAOException
	 */
	int findDriverOrderQuantity(int userId) throws DAOException;

	/**
	 * Find total number of orders, made by driver.
	 * 
	 * @param userId
	 *            ID of a driver
	 * @return list with all driver orders
	 * @throws DAOException
	 */
	List<Order> findDriverOrder(int userId) throws DAOException;

	/**
	 * Find total number of orders, made by client.
	 * 
	 * @param userId
	 *            ID of a client
	 * @return list with all client orders
	 * @throws DAOException
	 */
	List<Order> findClientOrder(int userId) throws DAOException;

	/**
	 * Edit a comment to the trip.
	 * 
	 * @param orderId
	 *            ID of an order, which comment should be edited
	 * @param newComment
	 *            new comment, which must be addet to the order
	 * @throws DAOException
	 */
	void editClientComment(int orderId, String newComment) throws DAOException;

	/**
	 * Edit a comment to the trip.
	 * 
	 * @param orderId
	 *            ID of an order, which comment should be edited
	 * @param newComment
	 *            new comment, which must be addet to the order
	 * @throws DAOException
	 */
	void editDriverComment(int orderId, String newComment) throws DAOException;

	/**
	 * Appends a new order to rhe database.
	 * 
	 * @param order
	 *            order with all necessary data
	 * @return
	 * @throws DAOException
	 */
	boolean appendOrder(Order order) throws DAOException;

	/**
	 * Add new comment, to the new order.
	 * 
	 * @param driverId
	 *            ID of a driver, who comment shold be added
	 * @param comment
	 *            comment, which should be added
	 * @return
	 * @throws DAOException
	 */
	int addCommentClient(int driverId, String comment) throws DAOException;

	/**
	 * Add new comment, to the new order.
	 * 
	 * @param clientId
	 *            ID of a client, who comment shold be added
	 * @param comment
	 *            comment, which should be added
	 * @return
	 * @throws DAOException
	 */
	int addCommentDriver(int clientId, String comment) throws DAOException;
}
