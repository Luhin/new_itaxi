package com.epam.itaxi.dao;

import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.DAOException;

/**
 * Connects to the database, and make some actions, which is relative to the all
 * types of users.
 * 
 * @author Uladzimir Luhin
 *
 */
public interface IUserDAO {
	/**
	 * Authorization of the user in the system.
	 * 
	 * @param login
	 * @param password
	 * @return if login and password are correct, return object User whith all
	 *         necessary data.
	 * @throws DAOException
	 */
	User authorizeUser(String login, String password) throws DAOException;

	/**
	 * Find user name by user ID;
	 * 
	 * @param userId
	 *            ID of a user
	 * @return String witch contains user name.
	 * @throws DAOException
	 */
	String findUserName(int userId) throws DAOException;

	/**
	 * Check login for the unique. Compare login to the database logins.
	 * 
	 * @param login
	 *            login which must be checked
	 * @return
	 * @throws DAOException
	 */
	boolean checkMatchExistLogin(String login) throws DAOException;

	/**
	 * Check password for id identity to the user.
	 * 
	 * @param userId
	 *            ID of a user
	 * @param password
	 *            password which must be checked
	 * @return
	 * @throws DAOException
	 */
	boolean chekUserPassword(int userId, String password) throws DAOException;

	/**
	 * Find rating of a user;
	 * 
	 * @param userId
	 *            ID of a user
	 * @return user rating
	 * @throws DAOException
	 */
	double findUserRating(int userId) throws DAOException;

	/**
	 * Check banned user or not.
	 * 
	 * @param userId
	 *            ID of a user
	 * @return if user banned - return true, if not return false
	 * @throws DAOException
	 */
	boolean isBanned(int userId) throws DAOException;

	/**
	 * Ban user. Banned user cannot make some actions.
	 * 
	 * @param userId
	 *            ID of a user who must be banned
	 * @throws DAOException
	 */
	void banUser(int userId) throws DAOException;

	/**
	 * Unban user
	 * 
	 * @param userId
	 *            ID of a user, who must be unbanned
	 * @throws DAOException
	 */
	void unBanUser(int userId) throws DAOException;

}
