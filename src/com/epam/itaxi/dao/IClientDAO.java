package com.epam.itaxi.dao;

import java.util.List;

import com.epam.itaxi.entity.User;
import com.epam.itaxi.exception.DAOException;

/**
 * Connects to the database, and make some actions, relative to the client.
 * 
 * @author Uladzimir Luhin
 *
 */
public interface IClientDAO {
	/**
	 * Determines current client position.
	 * 
	 * @return String, which contains current street.
	 * @throws DAOException
	 */
	String determineClientPosition() throws DAOException;

	/**
	 * Find client money amount on his acount.
	 * 
	 * @param clientId
	 *            ID of a client, who balance will be checked
	 * @return money amount of a client
	 * @throws DAOException
	 */
	int checkClientBalance(int clientId) throws DAOException;

	/**
	 * Checks client card number for identity with entered card number.
	 * 
	 * @param clientId
	 *            ID of a client
	 * @param cardNumber
	 *            - entered card number, which should be checked
	 * @return if card numbers a equals - return true, else return false
	 * @throws DAOException
	 */
	boolean checkCardNumber(int clientId, int cardNumber) throws DAOException;

	/**
	 * Repelnish balance of a client. Add given sum to the client money amount.
	 * 
	 * @param clientId
	 *            ID of a client
	 * @param replenishSum
	 *            sum which must be add to the client account
	 * @throws DAOException
	 */
	void replenishBalance(int clientId, int replenishSum) throws DAOException;

	/**
	 * Add a new client to the database.
	 * 
	 * @param name
	 *            client name
	 * @param login
	 *            client login
	 * @param password
	 *            client password
	 * @param email
	 *            client Email
	 * @param cardNum
	 *            client card number
	 * @return ID of a new client
	 * @throws DAOException
	 */
	public int signUpClient(String name, String login, String password, String email, String cardNum)
			throws DAOException;

	/**
	 * Rate client after drive.
	 * 
	 * @param clientId
	 *            client ID
	 * @param rating
	 *            mark, which was given to client
	 * @throws DAOException
	 */
	void rateClient(int clientId, int rating) throws DAOException;

	/**
	 * Find all clients from the database.
	 * 
	 * @return List with all clients.
	 * @throws DAOException
	 */
	List<User> findAllClients() throws DAOException;
}
