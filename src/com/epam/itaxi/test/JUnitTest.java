package com.epam.itaxi.test;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import com.epam.itaxi.connection_pool.ConnectionPool;
import com.epam.itaxi.connection_pool.ProxyConnection;
import com.epam.itaxi.exception.ConnectionPoolException;

public class JUnitTest {
	private final static String DB_PROPERTIES_PATH = "src/property/database.properties";

	@Test
	public void dataBasePropertiesExists() {
		File file = new File(DB_PROPERTIES_PATH);
		Assert.assertTrue(file.exists());
	}

	@Test
	public void createConnectionPool() {
		ConnectionPool connectionPool = ConnectionPool.getInstance();
		Assert.assertNotNull(connectionPool);
	}

	@Test(timeout = 2000)
	public void takeAndGiveBackConnectionTime() throws ConnectionPoolException {
		ConnectionPool connectionPool = ConnectionPool.getInstance();
		ProxyConnection connection = connectionPool.takeConnection();
		connectionPool.giveBackConnection(connection);
	}
}
