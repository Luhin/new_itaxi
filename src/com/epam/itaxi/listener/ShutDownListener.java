package com.epam.itaxi.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.itaxi.connection_pool.ConnectionPool;
import com.epam.itaxi.exception.ConnectionPoolException;
import com.epam.itaxi.exception.ProxyConnectionException;

/**
 * Application Lifecycle Listener implementation class ShutDownListener
 *
 */
@WebListener
public class ShutDownListener implements ServletContextListener {
	private static Logger logger = LogManager.getLogger();

	/**
	 * Closes all connections from the ConnectionPool, when the application will
	 * be shut down
	 * 
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		try {
			ConnectionPool.getInstance().clearConnectionPool();
		} catch (ProxyConnectionException | ConnectionPoolException e) {
			logger.error("Connetion not closed", e);
		}
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
	}
}
