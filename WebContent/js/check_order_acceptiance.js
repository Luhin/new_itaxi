;
function createXMLHttpRequest() {
	var request = false;
	if (window.XMLHttpRequest) {
		request = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		request = new ActiveXObject("Msxml2.XMLHTTP");
	}
	return request;
};
var request = createXMLHttpRequest();
function getAnswer() {
	if (request.readyState == 4 && request.status == 200) {
		var orderStatus = document.getElementById("orderStatus");
		orderStatus.style.display = "none";
		if (request.responseText == "noCars") {
			document.getElementById("noCarsDiv").style.display = "block";
		} else if (request.responseText == "orderCancelled") {
			return;
		} else {
			var arr = request.responseText.split("&");
			document.getElementById("orderAccepted").style.display = "block";
			document.getElementById("awaitingTime").innerHTML = arr[0];
			document.getElementById("driverName").innerHTML = arr[1];
			document.getElementById("rating").innerHTML = arr[2];
			document.getElementById("carModel").innerHTML = arr[3];
			document.getElementById("price").innerHTML = arr[4];
		}
	}
}
function checkOrderAcceptiance() {
	var url = document.getElementById("userURL").childNodes[0].nodeValue;
	request.open("POST", url, true);
	request.setRequestHeader("Content-Type",
			"application/x-www-form-urlencoded");
	request.send("command=wait-accept-order");
	request.onreadystatechange = getAnswer;
};
window.onload = checkOrderAcceptiance();