<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="custom-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="style/style.css" />
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle var="loc" basename="property.locale" />
<fmt:message var="clientLocation" bundle="${loc}"
	key="locale.client.location" />
<fmt:message var="makeOrdTitle" bundle="${loc}"
	key="locale.title.make.order" />
<fmt:message var="destTo" bundle="${loc}"
	key="locale.message.enter.destination" />
<fmt:message var="makeOrder" bundle="${loc}"
	key="locale.button.make.order" />
<fmt:message var="houseNumber" bundle="${loc}"
	key="locale.message.house.number" />
<fmt:message var="previousTrip" bundle="${loc}"
	key="locale.message.client.previous.trip" />
<fmt:message var="selectNewAddress" bundle="${loc}"
	key="locale.message.client.or.new" />
<fmt:message var="orderError" bundle="${loc}"
	key="locale.error.make.order" />
<title>${makeOrdTitle }</title>
</head>
<body>
	<%@ include file="include/header.jsp"%>
	<div class="content">
	${clientLocation }
	<c:out value="${sessionScope.currentStreet}" />
	&nbsp
	<c:out value="${sessionScope.currentHouseNumber}" />
	<br />
	<c:if test="${not empty sessionScope.previousAddress}">
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="send_order" />
			<input type="hidden" name="pageUnique" value="${sessionScope.pageUnique }"/> <input
				type="hidden" name="destination"
				value="${sessionScope.previousAddress }" /> <input type="hidden"
				name="houseNumber" value="${sessionScope.previousNumber }" />
			${previousTrip } <input class="btn btn-primary" type="submit" 
				value="${sessionScope.previousAddress } ${sessionScope.previousNumber }" />
			<br /> ${selectNewAddress }
		</form>
	</c:if>
	<form action="Controller" method="post">
		<input type="hidden" name="command" value="send_order" />
		<input type="hidden" name="pageUnique" value="${sessionScope.pageUnique }"/>
		<c:if test="${not empty requestScope.invalidRegistrData}">
			<span class="error">${requestScope.invalidRegistrData }</span>
			<br>
		</c:if>
		${destTo }
		<ctg:dest-choice streetList="${sessionScope.streetList}" />
		${houseNumber } <input type="text" name="houseNumber" size="5px"
			required pattern="[0-9]{1,3}" value="" /> <input type="submit" class="btn btn-primary"	
			value="${makeOrder }" />
	</form>
	<c:choose>
		<c:when test="${not empty requestScope.makeOrderError}">
			<div style="display: block">
		</c:when>
		<c:otherwise>
			<div style="display: none">
		</c:otherwise>
	</c:choose>
	<span class="error">${orderError }</span>
	</div>
	</div>
	<%@ include file="include/footer.jsp"%>
</body>
</html>