<fmt:message var="makeOrder" bundle="${loc}"
	key="locale.make.order" />
	<fmt:message var="showProfile" bundle="${loc}"
	key="locale.show.profile" />
<div>
	<form action="Controller" method="post">
		<input type="hidden" name="command" value="create_order" /> <input class="btn btn-primary"
			type="submit" value="${makeOrder }" />
	</form>
	<form action="Controller" method="post">
		<input type="hidden" name="command" value="show_client_profile" /> <input class="btn btn-info"
			type="submit" value="${showProfile }" />
	</form>
</div>