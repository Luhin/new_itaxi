<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<fmt:message var="showDriver" bundle="${loc}"
	key="locale.show.driver" />
	<fmt:message var="showClient" bundle="${loc}"
	key="locale.show.client" />
	<fmt:message var="showStatistic" bundle="${loc}"
	key="locale.show.statistic" />
<fmt:message var="enterName" bundle="${loc}" key="locale.entername" />
<fmt:message var="enterLogin" bundle="${loc}" key="locale.user.field" />
<fmt:message var="enterPassword" bundle="${loc}" key="locale.pass.field" />
<fmt:message var="repeatPassword" bundle="${loc}" key="locale.repeatpassword" />
<fmt:message var="enterEmail" bundle="${loc}" key="locale.enteremail" />
<fmt:message var="carModel" bundle="${loc}" key="locale.registration.car.model" />
<fmt:message var="nameInfo" bundle="${loc}" key="locale.message.name.info" />
<fmt:message var="loginInfo" bundle="${loc}" key="locale.message.login.info" />
<fmt:message var="passInfo" bundle="${loc}" key="locale.message.pass.info" />
<fmt:message var="emailInfo" bundle="${loc}" key="locale.message.email.info" />
<fmt:message var="carModelinfo" bundle="${loc}" key="locale.car.model.info" />
<fmt:message var="send" bundle="${loc}" key="locale.button.send" />
<fmt:message var="requiredFieldMessage" bundle="${loc}" key="locale.message.required.field" />
<fmt:message var="successOperation" bundle="${loc}" key="locale.message.success.operation" />
<fmt:message var="signUpDriver" bundle="${loc}" key="locale.button.registr.driver" />
<fmt:message var="requiredFieldChar" bundle="${loc}" key="locale.message.required.character" />

<div>
	<form action="Controller" method="post">
		<input type="hidden" name="command" value="show-all-clients" /> <input class="btn btn-primary"
			type="submit" value="${showClient }" />
	</form>
	<form action="Controller" method="post">
		<input type="hidden" name="command" value="show-all-drivers" /> <input class="btn btn-primary"
			type="submit" value="${showDriver }" />
	</form>
	<input id="show_div" class="btn btn-info" type="button" value="${signUpDriver }" />
	<c:if test="${requestScope.successOperation}">
		<h1>${successOperation }</h1>
	</c:if>
		</div>
		<c:choose>
  		<c:when test="${not empty requestScope.invalidRegistrData}">
   			 <div id="hidden_div" style="display:block">
 		 </c:when>
 		<c:otherwise>
    		<div id="hidden_div" style="display:none">
   		</c:otherwise>
		</c:choose>
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="sign-up-driver" />
			<input type="hidden" name="pageUnique" value="${sessionScope.pageUnique }"/>
			<c:if test="${not empty requestScope.invalidRegistrData}">
			<span class="error">${requestScope.invalidRegistrData }</span>
			<br>
			</c:if>
			${enterName }${requiredFieldChar }<br>
			<input type="text" name="registrName" required pattern="[a-zA-Zа-яёА-ЯЁ\s]{2,20}" value="" /> ${nameInfo }
			<br>
			${enterLogin }${requiredFieldChar }<br>
			<input type="text" name="registrLogin" required pattern="[a-zA-Z0-9]{4,20}" value="" /> ${loginInfo }
			<br>
			${enterPassword }${requiredFieldChar }<br>
			<input type="password" name="registrPass" required pattern="[a-zA-Z0-9]{4,20}" value="" /> ${passInfo }
			<br>
			${repeatPassword }${requiredFieldChar }<br>
			<input type="password" name="registrRepeatPass" required pattern="[a-zA-Z0-9]{4,20}" value="" />
			<br>
			${enterEmail }${requiredFieldChar }<br>
			<input type="text" name="registrEmail" required pattern=".+@.+" value="" /> ${emailInfo }
			<br>
			${carModel }<br>
			<input type="text" name="registrCardModel" required pattern="[0-9a-zA-Zа-яА-Я]{1,20}" value="" /> ${carModelinfo }
			<br>
			${requiredFieldMessage }
			<br>
			<input class="btn btn-success"
				type="submit" value="${send }" />
		</form>
	<script src="js/show_hidden_div.js" ></script>
</div>