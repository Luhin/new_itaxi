<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="style/style.css" />
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle var="loc" basename="property.locale" />
<fmt:message var="myProfile" bundle="${loc}"
	key="locale.title.my.profile" />
<fmt:message var="name" bundle="${loc}" key="locale.message.name" />
<fmt:message var="rating" bundle="${loc}" key="locale.message.rating" />
<fmt:message var="balance" bundle="${loc}" key="locale.message.balance" />
<fmt:message var="replenishBalance" bundle="${loc}"
	key="locale.button.replenish.balance" />
<fmt:message var="rideQuantity" bundle="${loc}"
	key="locale.message.ride.count" />
<fmt:message var="showMyRides" bundle="${loc}"
	key="locale.button.show.rides" />
<fmt:message var="replenishSum" bundle="${loc}" key="locale.message.enter.sum.replenishment" />
<fmt:message var="enterCardNumber" bundle="${loc}" key="locale.message.enter.card.code" />
<fmt:message var="invalidCard" bundle="${loc}" key="locale.message.wrong.card.number" />
<fmt:message var="replenishBalance" bundle="${loc}" key="locale.button.replenish" />
<fmt:message var="successOperation" bundle="${loc}" key="locale.message.success.operation" />
<fmt:message var="money" bundle="${loc}" key="locale.message.money" />
<title>${myProfile }</title>
</head>
<body>
	<%@ include file="include/header.jsp"%>
	<div class="content">
	${name }${sessionScope.userName }
	<br> ${rating }${sessionScope.userRating }
	<br> ${balance }${sessionScope.clientBalance } ${money }
	<br>
	<input id="show_div" class="btn btn-primary" type="button" value="${replenishBalance }" />
	<c:choose>
  		<c:when test="${requestScope.invalidData}">
   			 <div id="hidden_div" style="display:block">
 		 </c:when>
 		<c:otherwise>
    		<div id="hidden_div" style="display:none">
   		</c:otherwise>
	</c:choose>
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="replenish-balance" /> 
			<input type="hidden" name="pageUnique" value="${sessionScope.pageUnique }"/>
			${replenishSum }
			<br>
			<input
				type="text" name="replenishSum" required
					pattern="[0-9]{1,6}" value="" />
			<br>
			${enterCardNumber }
			<br>
			<c:if test="${requestScope.invalidData}">
			<span class="error">${invalidCard }</span>
			<br>
			</c:if>
			<input
				type="text" name="cardNumber" required
					pattern="[0-9]{3,3}" value="" />
			<br>
			<input class="btn btn-success"
				type="submit" value="${replenishBalance }" />
		</form>
	</div>
	<br>
	${rideQuantity }${sessionScope.rideQuantity }
	<form action="Controller" method="post">
		<input type="hidden" name="command" value="show-client-orders" /> <input class="btn btn-primary"
			type="submit" value="${showMyRides }" />
	</form>
	<c:if test="${requestScope.successOperation}">
		<h1>${successOperation }</h1>
	</c:if>
	</div>
	<%@ include file="include/footer.jsp"%>
	<script src="js/show_hidden_div.js" ></script>
</body>
</html>