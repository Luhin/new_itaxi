<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="style/style.css" />
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle var="loc" basename="property.locale" />
<fmt:message var="myProfile" bundle="${loc}"
	key="locale.title.my.profile" />
<fmt:message var="name" bundle="${loc}" key="locale.message.name" />
<fmt:message var="rating" bundle="${loc}" key="locale.message.rating" />
<fmt:message var="successOperation" bundle="${loc}" key="locale.message.success.operation" />
<fmt:message var="driverCar" bundle="${loc}" key="locale.message.car" />
<fmt:message var="changeCar" bundle="${loc}" key="locale.message.change.car" />
<fmt:message var="newCar" bundle="${loc}" key="locale.message.enter.new.car" />
<fmt:message var="enterPassword" bundle="${loc}" key="locale.message.enter.password" />
<fmt:message var="wrongPassword" bundle="${loc}" key="locale.message.wrong.password" />
<fmt:message var="changeCar" bundle="${loc}" key="locale.button.change.car" />
<fmt:message var="orderQuantity" bundle="${loc}" key="locale.message.order.quantity" />
<fmt:message var="showMyorders" bundle="${loc}" key="locale.button.show.orders" />
<title>${myProfile }</title>
</head>
<body>
	<%@ include file="include/header.jsp"%>
	<div class="content">
	${name }${sessionScope.userName }
	<br> ${rating }${sessionScope.userRating }
	<br> ${driverCar }${sessionScope.driverCar }
	<br>
	<input id="show_div" class="btn btn-primary" type="button" value="${changeCar }" />
	<c:choose>
  		<c:when test="${requestScope.invalidData}">
   			 <div id="hidden_div" style="display:block">
 		 </c:when>
 		<c:otherwise>
    		<div id="hidden_div" style="display:none">
   		</c:otherwise>
	</c:choose>
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="change-car" /> 
			<input type="hidden" name="pageUnique" value="${sessionScope.pageUnique }"/>
			${newCar }
			<br>
			<input
				type="text" name="newCar" required
					pattern="[\w\d]{1,20}" value="" />
			<br>
			${enterPassword }
			<br>
			<c:if test="${requestScope.invalidData}">
			<span class="error">${wrongPassword }</span>
			<br>
			</c:if>
			<input
				type="password" name="password" required
					pattern="[\d\w]{4,20}" value="" />
					<br>
			<input class="btn btn-success"
				type="submit" value="${changeCar }" />
		</form>
	</div>
	<br>
	${orderQuantity }${sessionScope.rideQuantity }
	<form action="Controller" method="post">
		<input type="hidden" name="command" value="show-driver-orders" /> <input class="btn btn-primary"
			type="submit" value="${showMyorders }" />
	</form>
	<c:if test="${requestScope.successOperation}">
		<h1>${successOperation }</h1>
	</c:if>
	</div>
	<%@ include file="include/footer.jsp"%>
	<script src="js/show_hidden_div.js" ></script>
</body>
</html>