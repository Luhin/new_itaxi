<%@ page language="java" contentType="text/html; charset=utf-8"
	isErrorPage="true" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="style/style.css" />
<link rel="stylesheet" type="text/css" href="style/bootstrap.css" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle var="loc" basename="property.locale" />
<fmt:message var="error" bundle="${loc}" key="locale.error" />
<fmt:message var="onMain" bundle="${loc}" key="locale.on.main" />
<fmt:message var="errorTitle" bundle="${loc}" key="locale.title.error" />
<title>${errorTitle }</title>
</head>
<body>
	<div class="error_div">
		<h1>${error }</h1>
		<form action="${ pageContext.request.contextPath }/index.jsp"
			method="post">
			<input class="error_button btn btn-primary" type="submit" value="${onMain }" />
		</form>
	</div>
</body>
</html>